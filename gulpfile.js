const gulp = require('gulp');
const gulpsync = require('gulp-sync')(gulp);
const run = require('run-sequence').use(gulp);
const colors = require('colors');
const console = require('better-console');
const installTask = require('./tasks/install/install');
const fs = require('fs');
let browserSync;

process.env.BUILD_CONF = process.env.BUILD_CONF || 'local';
process.env.ROOT_DIR = process.env.ROOT_DIR || __dirname;
process.env.PACKAGE_DIR = process.env.PACKAGE_DIR || __dirname;


console.log(
    ('Build config:'.bold) + '\n' +
    (('Process dir: '.bold).blue) + (process.cwd()) + '\n' +
    (('ROOT_DIR: '.bold).blue) + process.env.ROOT_DIR + '\n' +
    (('BUILD_CONF: '.bold).blue) + process.env.BUILD_CONF + '\n',
);
gulp.task('install', installTask);

let buildCss = require('./tasks/build/css');
let buildMedia = require('./tasks/build/media');
let buildHTML = require('./tasks/build/html');
let developTask = require('./tasks/build/develop');

let devServer = require('./tasks/dev-server/server');

/*******************************
 Tasks
 *******************************/

gulp.task('hello', function() {
  console.log('========================================= '.green);
  console.log('=                                       = '.green);
  console.log('=  ██╗   ██╗██╗   ██╗ ██╗██╗████████╗   = '.green);
  console.log('=  ██║   ██║██║   ██║██╔╝██║╚══██╔══╝   = '.green);
  console.log('=  ██║   ██║██║   ████╔╝ ██║   ██║      = '.green);
  console.log('=  ██║   ██║██║   ██║██║ ██║   ██║      = '.green);
  console.log('=  ╚██████╔╝██║   ██║ ██╗██║   ██║      = '.green);
  console.log('=   ╚═════╝ ╚═╝   ╚═╝ ╚═╝╚═╝   ╚═╝      = '.green);
  console.log('=                                       = '.green);
  console.log('========================================= '.green);
  console.log('Gisauto UI Kit');
  console.log('========================================='.green);
});

gulp.task('build css', buildCss.cssSass);
gulp.task('convert svg to .sass', buildCss.cssInlineSvg);
gulp.task('convert fonts to .sass', buildCss.cssInlineFont);
gulp.task('images', '', buildMedia.images);
gulp.task('videos', '', buildMedia.videos);
gulp.task('fonts', '', buildMedia.fonts);
gulp.task('generate fonts as icons', '', buildMedia.iconFonts);
gulp.task('twig to html', '', buildHTML.twig);
gulp.task('json', '', buildHTML.json);
gulp.task('server', function() {
  browserSync = require('browser-sync');
  browserSync(require('./tasks/config/config')(process.env.BUILD_CONF || 'local').server);
});

gulp.task('watch', function() {
  let config = require('./tasks/config/config')(process.env.BUILD_CONF);
  gulp.watch([
    config.css.path.sass.src + config.css.path.sass.files,
    config.css.path.sass.exclude, './src/semantic/dist/semantic.css'], function(event) {
    run('build css');
  });
  gulp.watch(config.media.path.images.src + config.media.path.images.files, ['images']);
  gulp.watch(config.html.path.twig.src + config.html.path.twig.files, ['twig to html']);
  let timer;
  if (browserSync) {
    browserSync.watch('./src/**/*').
        on('change', function() {
          if (timer) {
            clearTimeout(timer);
          }
          timer = setTimeout(browserSync.reload, 1500);
        });
  }
});

gulp.task('mobile styles', '', developTask.mobileSass);
gulp.task('mobile styles --watch', function() {
  let config = require('./tasks/config/config')(process.env.BUILD_CONF);
  gulp.watch([
    config.css.path.sass.src + config.css.path.sass.files,
    config.css.path.sass.exclude, './src/semantic/dist/semantic.css'], ['set production config', 'mobile styles']);
});

gulp.task('build', gulpsync.sync([
  'hello',
  'images',
  'videos',
  'json',
  'fonts',
  'generate fonts as icons',
  'convert fonts to .sass',
  'twig to html',
  'build css',
  'server',
]));

gulp.task('default', gulpsync.sync(['build', 'watch']));

gulp.task('set production config', function() {
  process.env.BUILD_CONF = 'production';
});
gulp.task('build develop', gulpsync.sync([
  'set production config',
  'hello',
  'generate fonts as icons',
  'images',
  'videos',
  'json',
  'fonts',
  'convert svg to .sass',
  'convert fonts to .sass',
  'build css',
  'watch',
]));

/**
 * Продакшн сборка стилей и медиа файлов
 */
gulp.task('build production', gulpsync.sync([
  'set production config',
  'generate fonts as icons',
  'images',
  'videos',
  'json',
  'fonts',
  'convert svg to .sass',
  'convert fonts to .sass',
  'build css',
]));
