import $ from 'jquery';

$(function() {
  console.log('JS chat begin');
  $('.kit.btn-chat-open, .kit .close-chat').on('click', function(ev) {
    if ($(ev.target).is('.close-chat-dialog-mobile')) return;
    $('.kit.chat-body').toggleClass('hide show');
    $('.kit.btn-chat-open').toggleClass('hide show');
    $('.kit.chat-container').toggleClass('open');
    if ($(ev.target).is('.close-chat')) {
      $('.kit.chat-dialog-wrap').removeClass('show');
      $('.kit.chat-dialog-wrap').addClass('hide');
      $('.kit.chat-container').removeClass('full-screen');
      $('.kit.chat-nav .resize').removeClass('min');
      $('.kit.chat-nav .resize').addClass('max');
      $('.kit.author-talk.active').removeClass('active');
      $('.kit.chat-nav').css({'min-width': '', 'width': ''});
    }
  });

  $('.kit .open-search-form, .kit .close-search-form').
      on('click', function(ev) {
        $('.kit.chat-dialog-wrap .header').toggleClass('hide show');
        $('.kit.dialog-body').toggleClass('hide show');
        $('.kit.user-message').toggleClass('hide show');
        $('.kit.search-result-body').toggleClass('hide show');
        if ($(ev.target).is('.s-all')) {
          $('.kit .header.search .search-back').addClass('hide');
          $('.kit .header.search .search-back').removeClass('show');
          $('.kit .header.search .s-place.local').addClass('hide');
          $('.kit .header.search .s-place.local').removeClass('show');
          $('.kit .header.search .s-place.global').addClass('select');
          if (!$('.kit.chat-dialog-wrap').hasClass('show')) {
            $('.kit.chat-dialog-wrap').addClass('show');
            $('.kit.chat-dialog-wrap').removeClass('hide');
          }
          ;
        } else {
          $('.kit .header.search .s-place.local').addClass('show select');
          $('.kit .header.search .s-place.local').removeClass('hide');
          $('.kit .header.search .s-place.global').removeClass('select');
          $('.kit .header.search .search-back').addClass('show');
          $('.kit .header.search .search-back').removeClass('hide');
        }
      });

  $('.kit.chat-nav .resize').on('click', function(ev) {
    if ($(ev.target).is('.max')) {
      $('.kit.chat-container').addClass('full-screen');
      $('.kit.chat-nav').css({'min-width': '22rem', 'width': '22rem'});
    }
    ;
    if ($(ev.target).is('.min')) {
      $('.kit.chat-container').removeClass('full-screen');
      $('.kit.chat-nav').css({'min-width': '0', 'width': '6rem'});
    }
    ;
    $(this).toggleClass('max');
    $(this).toggleClass('min');
  });

  $('.kit .authors-link').on('click', function() {
    if ($(this).hasClass('active')) return;
    if (mobile()) {
      $('.kit.chat-nav').css({'min-width': '0', 'width': '0'});
    }
    $('.kit.author-talk.active').removeClass('active');
    $(this).find('.kit.author-talk').toggleClass('active');
    if ($('.kit.chat-dialog-wrap').hasClass('show')) return;
    $('.kit.chat-dialog-wrap').addClass('show');
    $('.kit.chat-dialog-wrap').removeClass('hide');
  });

  $('.kit .close-chat-dialog-mobile').on('click', function() {
    if (mobile()) {
      $('.kit.chat-nav').css({'min-width': '100vw', 'width': '100%'});
    }
  });

  $(window).resize(function() {
    if (mobile()) {
     //debugger;
      $('.kit.chat-nav').css({'min-width': '100vw', 'width': '100%'});
      if ($('.kit.chat-dialog-wrap').hasClass('show')) {
        $('.kit.chat-nav').css({'min-width': '0', 'width': '0'});
      };
    } else {
      $('.kit.chat-nav').css({'min-width': '', 'width': ''});
    }
  });

  $('.kit .close-chat-dialog').on('click', function() {
    if (mobile()) {
      $('.kit.chat-nav').css({'min-width': '100vw', 'width': '100%'});
    }
    $('.kit.author-talk.active').removeClass('active');
    $('.kit.chat-dialog-wrap').removeClass('show');
    $('.kit.chat-dialog-wrap').addClass('hide');
  });

  $('.kit.hint-hover .m-close').on('click', function() {
    $(this).parent('.kit.hint-hover').hide();
  });

  $('.kit.hint-call').on('click.hint-call', function(ev) {
    if ($(ev.target).is('.hint-call span') ||
        $(ev.target).is('.hint-call')) {
      $(this).find('.kit.hint-hover').show();
    }
  });

  $('.kit.chat-input .btn-attach').on('click', function() {
    $(this).toggleClass('open');
    $(this).toggleClass('close');
    $('.kit .overbox').toggleClass('active');
  });

  $('.kit .where-search .s-place').on('click', function() {
    if ($(this).hasClass('select')) return;
    $('.kit .where-search .s-place').toggleClass('select');
  });

  function mobile() {
    var scr_w = screen.width;
    var scr_h = screen.height;
    if (scr_w <= 768) return true;
    return false;
  };

});
