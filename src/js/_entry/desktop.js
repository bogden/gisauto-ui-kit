import $ from 'jquery';
import Sidebar from 'Modules/desktop/Sidebar';
import Lazyload from 'Modules/Accordion';
import Rating from 'Modules/Rating';


import '../initializers/accordion';
import '../initializers/tooltip';

class Desktop {
  constructor() {
    this.initSidebar();
  }

  initSidebar() {
    this.sidebar = new Sidebar();
    this.sidebar.init('#siteSidebar');
  }
}

$(function() {
  !window.hasOwnProperty('$$') && (window.$$ = {});
  window.$$.Desktop = new Desktop();
  console.log(window.$$.Desktop);
  Rating.load();
  window.$$.Rating.init('.ui.rating').initRatings();


});