import $ from 'jquery';
import LazyLoad from 'Modules/LazyLoad';
import Autoload from 'Modules/Autoload';
import ActionManager from 'Modules/ActionManager';
import UniqueId from '../../../environment/UniqueId';

import '../../../vue/init';
import '../../../vue/desktop/cabinet/sto/app';

$(function() {

  /**
   * Добавляем каждому объекту уникальный ID
   * с помощью свойства uniqueId
   */
  UniqueId.load();

  Autoload.load();
  ActionManager.load();

  window.$$.Autoload.onLoaded(function() {
    window.$$.lazyLoad = new LazyLoad();
    window.$$.lazyLoad.init('.lazy').addEvent().lazyload();
  });
  window.$$.ActionManager.
      init(document).
      initTriggers();

});