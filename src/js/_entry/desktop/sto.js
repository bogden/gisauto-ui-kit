import $ from 'jquery';
import '../desktop';
import GoogleMap from '../../api/GoogleMap';
import Dropdown from 'Modules/Dropdown';

class STO {
  constructor() {
    this.map = new GoogleMap('AIzaSyCyXrHqAhl6Ah2-jfNVPWM0LVo0d1hX_9s',
        function() {
          this.map.map('stoMap', {lat: -25.363, lng: 131.044}, 10);
          this.map.marker('test', {lat: -25.363, lng: 131.044}, {
            title: 'Тестовая метка',
          }).marker('test2', {lat: 0, lng: 0}, {
            title: 'Тестовая метка',
          });
        }.bind(this));
  }
}

$(function() {
  !window.hasOwnProperty('$$') && (window.$$ = {});
  window.$$.STO = new STO();

  Dropdown.load();
  window.$$.Dropdown.init('.ui.dropdown');
});