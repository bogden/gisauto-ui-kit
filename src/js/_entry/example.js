/**
 * Подключаем все модули из папки examples
 */

import '../initializers/kit-layout';
import '../initializers/actions';
import '../initializers/semantic';
import '../initializers/alert';
import '../initializers/dropdowns';
import '../initializers/preloader';
import '../initializers/mobile-sidebar';
import '../initializers/mobile-bar';
import '../initializers/text-collapse';
import '../initializers/forms';
import '../desktop/timepicker';
import '../initializers/tooltip';
import '../initializers/appModals';
import '../initializers/accordion';
