import $ from 'jquery';
import GoogleMap from '../../api/GoogleMap';

$(function() {
  if (!window.$$) {
    window.$$ = {};
  }
  let map = new GoogleMap('AIzaSyCyXrHqAhl6Ah2-jfNVPWM0LVo0d1hX_9s',
    function() {
      map.map('map', {lat: -25.363, lng: 131.044}, 4);
      map.marker('test', {lat: -25.363, lng: 131.044}, {
        title: 'Тестовая метка',
      }).marker('test2', {lat: 0, lng: 0}, {
        title: 'Тестовая метка',
      });
    });

  let mapModal = new GoogleMap('AIzaSyCyXrHqAhl6Ah2-jfNVPWM0LVo0d1hX_9s',
    function() {
      mapModal.modal('mapModal', {lat: -25.363, lng: 131.044}, 4);
    });
});
