import $ from 'jquery';
import LazyLoad from 'Modules/LazyLoad';
import Autoload from 'Modules/Autoload';
import Authorizer from 'Modules/Authorizer';
import ActionManager from 'Modules/ActionManager';
import Input from 'Modules/Input';
import Modal from 'Modules/Modal';
import LocationSelect from 'Modules/LocationSelect';
import Alert from 'Modules/Alert';
import Feedback from 'Modules/Feedback';
import UniqueId from '../environment/UniqueId';
import Profile from 'Modules/mobile/Profile';
import Rating from 'Modules/Rating';
import Sidebar from 'Modules/mobile/Sidebar';

import commonFunctions from '../mobile/common/_common.js';
import '../mobile/common/mobile-bar';
import '../mobile/common/preloader';
import '../mobile/common/semantic';
import '../mobile/common/text-collapse';
import '../mobile/common/forms';
import '../mobile/common/actions';
import '../mobile/common/tab';
import '../initializers/tooltip';

/**
 * Плагины jQuery
 */
import '../plugins/ajaxCache';
/**
 * Плагины Semantic
 */

import '../initializers/accordion';
import '../initializers/uncover';

$(function() {

  /**
   * Добавляем каждому объекту уникальный ID
   * с помощью свойства uniqueId
   */
  UniqueId.load();

  commonFunctions();

  Modal.load();
  Autoload.load();
  ActionManager.load();
  Input.load();
  LocationSelect.load();
  Alert.load();
  Feedback.load();
  Profile.load();
  Rating.load();
  Sidebar.load().init('#sidebar').initSubmenu();

  window.$$.Autoload.onLoaded(function() {
    window.$$.Rating.init('.ui.rating').initRatings();
    window.$$.lazyLoad = new LazyLoad();
    window.$$.lazyLoad.init('.lazy').addEvent().lazyload();
  });

  window.$$.Authorizer = new Authorizer();

  window.$$.ActionManager.
      init(document).
      initTriggers().
      addCommand('open authorization modal', window.$$.Authorizer.authorizeModal.bind(window.$$.Authorizer)).
      addCommand('password input control', window.$$.Input.passwordControl.bind(window.$$.Input)).
      addCommand('select location', window.$$.LocationSelect.selectLocation.bind(window.$$.LocationSelect)).
      addCommand('init feedback', window.$$.Feedback.initForm.bind(window.$$.Feedback)).
      addCommand('open feedback', window.$$.Feedback.openFeedbackModal.bind(window.$$.Feedback)).
      addCommand('open profile menu', window.$$.Profile.toggleProfileMenu.bind(window.$$.Profile)).
      addCommand('erase input control', window.$$.Input.eraseControl.bind(window.$$.Input)).
      addCommand('toggle sidebar', window.$$.Sidebar.toggle.bind(window.$$.Sidebar)).
      addCommand('close sidebar', window.$$.Sidebar.hide.bind(window.$$.Sidebar)).
      addCommand('open sidebar', window.$$.Sidebar.show.bind(window.$$.Sidebar));

});
