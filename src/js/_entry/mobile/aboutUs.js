import $ from 'jquery';
import '../mobile';
import Vue from 'vue/dist/vue.min';
import VueFilters from 'Modules/VueFilters';
import NavStack from 'Modules/NavStack';

const AboutUs = Vue.extend({
  el: '#aboutUs',
  mixins: [VueFilters],
  data() {
    return {
      screens: null,
      loading: true
    };
  },
  computed: {
    screen: {
      get: function() {
        return this.screens ? this.screens.getActive() : 'index';
      },
      set: function(screen) {
        this.hasOwnProperty('screens') && this.screens.push(screen);
      },
    },
  },
  mounted() {
    this.screens = new NavStack('#aboutUs');
    this.go('index');
  },
  methods: {
    go(screen) {
      this.screen = screen;
    },
  },
});

$(function() {
  !window.hasOwnProperty('$$') && (window.$$ = {});
  window.$$.aboutUs = new AboutUs();
});
