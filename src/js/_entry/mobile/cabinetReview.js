import '../mobile';
import $ from 'jquery';
import Vue from 'vue/dist/vue.min';
import RatingDirective from 'Modules/directives/rating'

let Registration = Vue.extend({
  el: '#cabinetReviews',
  mixins: [],
  directives: {
    'rating': RatingDirective
  },
  data() {
    return {
    };
  },
  created() {
  },
  updated() {
  },
  methods: {
  },
  watch: {
  },
});

$(function() {
  window.$$.tyres = new Registration();
});