import '../mobile';
import $ from 'jquery';
import Vue from 'vue/dist/vue.min';
import VueFilters from 'Modules/VueFilters';
import DropdownDirective from 'Modules/directives/dropdown';

let ProfileSettings = Vue.extend({
  el: '#profileSettings',
  mixins: [VueFilters],
  directives: {
    'dropdown': DropdownDirective,
  },
  data() {
    return {
      editPasswordMode: false,
    };
  },
  created() {

  },
  updated() {
    $(document).trigger('lazyload');
  },
  methods: {
  },
  watch: {
  },
});

$(function() {
  window.$$.tyres = new ProfileSettings();
});