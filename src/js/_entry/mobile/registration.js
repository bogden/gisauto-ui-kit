import '../mobile';
import $ from 'jquery';
import Vue from 'vue/dist/vue.min';
import VueNavStack from 'Modules/VueNavStack';

let Registration = Vue.extend({
  el: '#registrationContext',
  mixins: [VueNavStack],
  data() {
    return {
      screensSelector: '#registrationScreens',
      userKind: null,
    };
  },
  created() {
    this.$data.screensSelector = '#registrationScreens';
  },
  updated() {
  },
  methods: {
    setStartKind(kind = null){
      !this.$data.userKind && (this.$data.userKind = kind);
    },
    startRegistration(kind){
      this.$data.userKind = kind;
      this.go('registrationForm');
    }
  },
  watch: {
    userKind(newValue, oldValue){
    }
  },
});

$(function() {
  window.$$.tyres = new Registration();
});