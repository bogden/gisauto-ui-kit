import '../mobile.js';
import $ from 'jquery';
import Vue from 'vue/dist/vue.min';
import VueFilters from 'Modules/VueFilters';
import VueNavStack from 'Modules/VueNavStack';
import CarMixin from 'Modules/VueCar';
import Validator from 'Modules/Validator';
import Steps from '../../vue/mobile/request-vin-steps.vue';
import Parts from '../../vue/mobile/parts-looper.vue';
import Form from 'Modules/Form';
import Dropdown from 'Modules/Dropdown';
import Input from 'Modules/Input';
import InputDirective from 'Modules/directives/input'
import Router from 'Modules/Router';

let RequestToSpare = Vue.extend({
  el: '#requestToSpare',
  mixins: [VueFilters, VueNavStack, CarMixin],
  directives: {
    'input': InputDirective
  },
  data() {
    return {
      $form: $('.form-request-to-spare'),
      api: {
        'parts helper': decodeURI(
            Router.generate('part_helper', {searchString: '{query}'}, window.location.host)),
        'city search': decodeURI(
            Router.generate('api-get-cities-by-query', {query: '{query}'}, window.location.host)),
      },
      dropdown: new Dropdown(),
      modelDropdown: null,
      vin: '',
      customer: {
        name: '',
        email: '',
        city: '',
        phone: '',
        info: '',
      },
      state: {
        created: false,
        mounted: false,
        partsAvailable: false,
        iterator: 0,
        step: 1,
      },
      stepNumber: 1,
      manufacturers: JSON.parse(localStorage.getItem('manufacturers')) || [],
      models: [],
      parts: [],
      errors: {},
      seller: 0,
      screensSelector: '#requestToSpareForm',
    };
  },
  created() {
    /** Изменяется список моделей авто **/

    this.$on('getModelsByManufacturer', function(data) {
      let $select = $('#customer_request_carModel').html('');
      this.car.reset('model');
      data.forEach(function(item) {
        $select.append(
            '<option value="' + item.value + '">' + item.name + '</option>',
        );
      });
      this.initModelDropdown(data);
      this.updatedCarScreen();
    }.bind(this));

    this.$on('partsAvailable', function() {
      this.setState({partsAvailable: true});
    }.bind(this));

    this.$on('partsUnavailable', function() {
      this.setState({partsAvailable: false});
    }.bind(this));

    this.$on('updateParts', function() {
    }.bind(this));
    $(document).on('request-to-spare.complete', function(e, params) {
      this.seller = params.seller;
      alert('Найдено ' + params.seller + ' продавцов');
    }.bind(this));

  },
  mounted() {
    this.initForm().go('vin');
    (new Input()).
        initNumbersInput('input[type="number"]').
        eraseControl('.form-control-erase').
        passwordControl('.form-control-password').
        phoneControl('.form-control-phone').
        emailControl('.form-control-email');
    Input.fixtures();
    $('.kit.mobile.request-to-spare.opacity').removeClass('opacity');
    $('.ui.calendar.year').calendar({
      minDate: new Date(10000),
      maxDate: new Date(),
      type: 'year',
      firstDayOfWeek: 1,
      popupOptions: {
        position: 'top left',
        lastResort: 'bottom left',
        prefer: 'opposite',
        hideOnScroll: true
      },
      onChange: function (date, text, mode) {
          this.car.info.year = text;
      }.bind(this)
    });
  },
  updated() {
    this.screens.getActive() === 'vin' && this.updatedVinScreen();
    this.screens.getActive() === 'car' && this.updatedCarScreen();
    this.screens.getActive() === 'parts' && this.updatedPartsScreen();
    this.screens.getActive() === 'customer' && this.updatedCustomerScreen();
  },
  methods: {
    onPush(screen) {
      screen === 'vin' && this.initVinScreen();
      screen === 'car' && this.initCarScreen();
      screen === 'parts' && this.initPartsScreen();
      screen === 'customer' && this.initCustomerScreen();
      $(document).trigger('init.inputs');
    },
    /**
     * Ининциализация экрана ввода VIN\FRAME номера.
     * На данном экране пользователь может указать
     * VIN\FRAME номер своего авто и заполнить все необходымые
     * поля данных автомобиля автоматически.
     * @return {RequestToSpare}
     */
    initVinScreen() {
      this.updatedVinScreen();
      return this;
    },
    updatedVinScreen() {
      this.stepNumber = 1;
      return this;
    },

    /**
     * Инициализация экрана ввода данных автомобиля.
     * На данном экране пользователь указывает:
     * - марку
     * - модель
     * - кузов
     * - мотор
     * - год выпуска
     * @return {RequestToSpare}
     */
    initCarScreen() {

      this.updatedCarScreen();
      return this;
    },

    /**
     * Обновление экрана выбора автомобиля
     * @return {RequestToSpare.methods}
     */
    updatedCarScreen() {
      this.initDropdown();
      this.initManufacturerDropdown();
      this.stepNumber = 1;
      return this;
    },

    /**
     * Инициализация экрана ввода запчастей.
     * На данном экране пользователь указывает
     * необходимые запчасти и их тип (Новая или Контрактная).
     * @return {RequestToSpare}
     */
    initPartsScreen() {
      this.updatedPartsScreen();
      return this;
    },
    updatedPartsScreen() {
      this.initDropdown();
      this.stepNumber = 2;
      return this;
    },

    /**
     * Инициализация экрана заполнения данных пользователя.
     * На данном экране необходимо заполнить основные
     * поля с информацие о пользователе.
     * @return {RequestToSpare}
     */
    initCustomerScreen() {
      this.updatedCustomerScreen();
      return this;
    },
    updatedCustomerScreen() {
      this.initDropdown();
      this.initCityDropdown();
      this.stepNumber = 3;
      return this;
    },

    initForm() {
      this.form = (new Form('#requestToSpareForm', {
        finder: {
          prefix: 'customer_request[',
          postfix: ']',
        },
      })).initForm().setPhoneField('#customer_request_customerPhone');
      return this;
    },

    initDropdown() {
      (new Dropdown()).init('.ui.dropdown.auto-init', {
        onChange: Dropdown.onChange,
      });
    },

    initManufacturerDropdown() {
      (new Dropdown()).init('.manufacturer-dropdown', {
        onChange: Dropdown.onChange,
      });
    },

    initModelDropdown(values = []) {
      $('.ui.dropdown.car-dropdown').removeAttr('disabled');
      this.modelDropdown = (new Dropdown()).init('.ui.dropdown.car-dropdown', {
        onChange: Dropdown.onChange,
      }).apply('change values', values);
      return this;
    },

    initCityDropdown() {
      this.cityDropdown = (new Dropdown()).initCityDropdown('.ui.dropdown.city-dropdown');
      return this;
    },

    /**
     * Проверить значение переменной
     * @param value
     * @param type
     * @param options
     */
    validator(value, type, options = {}) {
      return (new Validator(value, type)).check(value, type, options);
    },

    /**
     * Установить состояние
     * @param state
     * @return {RequestToSpare.methods}
     */
    setState(state) {
      this.state = $.extend(this.state, state);
      this.state.iterator++;
      return this;
    },

    /**
     * Верно ли заполнен список запчастей
     * @return {boolean}
     */
    partsAvailable() {
      return !!this.state.partsAvailable;
    },

    /**
     * Верно ли заполненны данные пользователя
     * оставляющего запрос на детали по VIN\FRAME
     * @return {*}
     */
    customerAvailable() {
      return this.validator(this.customer.name, 'notBlank')
          && this.validator(this.customer.email, 'email')
          && this.validator(this.customer.city, 'notBlank')
          && this.validator(this.customer.phone, 'phone');
    },

    /**
     * Получить информацию об автомобиле по VIN номеру
     * - Если информация не получена то вывести ошибку
     * - Если информация получена в полном объеме,
     *   Сделать кнопку "Далее" активной.
     * @param vin
     */
    getInfoByVin(vin) {
      vin && (this.car.getInfoByVin(vin).done(function(response) {
        this.setState({
          validVin: response.success,
          invalidVin: !response.success,
        });
      }.bind(this)));
    },
  },

  /**
   * Обработка изменения состояние
   * некоторых данных
   */
  watch: {
    'car.info.vin'(newValue, oldValue) {
      newValue !== oldValue && function() {
        this.car.info.vin = newValue.toUpperCase();
        (newValue.length < 8 || this.validator(newValue, 'vin'))
            ? this.setState({invalidVin: false})
            : this.setState({invalidVin: true});
      }.bind(this)();
    },
  },

  /**
   * Компоненты экземпляра
   * - Индикатор прогресса\шагов
   * - Коллекция запчайтей
   */
  components: {
    'steps': Steps,
    'parts-looper': Parts,
  },

});

$(function() {
  !window.hasOwnProperty('$$') && (window.$$ = {});
  window.$$.RequestToSpare = (new RequestToSpare());
});