import $ from 'jquery';
import Vue from 'vue/dist/vue.min';
import '../mobile';
import Search from '../../vue/mobile/search.vue';
import Form from 'Modules/Form';
import VueState from 'Modules/VueState';
import Accordion from 'Modules/Accordion';
import VueNavStack from 'Modules/VueNavStack';
import Dropdown from 'Modules/Dropdown';
import Router from 'Modules/Router';
import '../../plugins/uncover';
import '@fancyapps/fancybox';
import 'owl.carousel';

let PartsSearch = Vue.extend({
  el: '#searchByNumber',
  components: {
    'search': Search,
  },
  mixins: [VueState, VueNavStack],
  data() {
    return {
      endpoint: '/latest',
      partQueryCache: {},
      searchQuery: '',
      parts: [],
      items: JSON.parse(localStorage.getItem('latestSearches')) || [],
      stateHistory: [],
      selectedPart: null,
      screensSelector: '#searchByNumberScreens',
    };
  },
  created() {
    this.defineState({
      search: false,
    });
    this.$on('search', function(search) {
      this.setState({search: search});
    });
    this.$on('push', function(screen) {
    });
  },
  mounted() {
    $().fancybox({
      selector: '[data-open-modal="order"]',
      afterShow: this.openOrderForm,
    });
    this.accordion = (new Accordion()).init('.ui.accordion');
  },
  updated() {
  },
  methods: {
    getProductInformation(item) {
      return $.getJSON(Router.generate('get_item_order_modal', {item: item}, window.location.host), {item: item}, function(data) {
        this.selectedItemData = data;
      }.bind(this));
    },

    openShopInfo(item) {
      let response = this.getProductInformation(item);
      response.promise().done(function(responseData) {
        if (responseData.hasOwnProperty('data')) {
          let data = responseData.data;
          let $context = $('<div class="screen-wrap">' + data.htmlAbout + data.htmlFeedbacks + '</div>');
          this.preloader.hide();
          this.accordion.setContext($context).init('.ui.accordion');
          $('.cover', $context).uncover();
          this.screens.add('shop', $context).push('shop');
        }
      }.bind(this)).fail(function(e) {
        this.preloader.hide();
        alert('Произошла ошибка!');
      }.bind(this));
    },

    openOrderForm(fancyboxInstance) {
      /**
       * Инициализация формы заказа
       */
      let form = new Form('.form-order', {
        finder: {
          prefix: 'order_search_by_num_mobile[',
          postfix: ']',
        },
      });
      form.initForm();
      $('#orderFormCarousel',fancyboxInstance.current.$content).owlCarousel({
        items: 1,
        margin: 10,
        autoHeight: true,
        loop: false,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
      });
      (new Dropdown()).initCityDropdown('.ui.dropdown.city-dropdown');

    },

    openPartInfo() {
      if (this.screens.getActive() !== 'partInfo') {
        this.screens.push('partInfo');
      } else {
        this.screens.push('default');
      }
    },

    openFilters() {
      if (this.screens.getActive() !== 'filters') {
        this.screens.push('filters');
      } else {
        this.screens.push('default');
      }
    },
  },
});

$(function() {
  !window.hasOwnProperty('$$') && (window.$$ = {});
  window.$$.partsSearch = new PartsSearch();
});
