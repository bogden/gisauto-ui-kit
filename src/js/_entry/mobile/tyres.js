import '../mobile';
import $ from 'jquery';
import Vue from 'vue/dist/vue.min';
import VueFilters from 'Modules/VueFilters';
import VueNavStack from 'Modules/VueNavStack';
import DropdownDirective from 'Modules/directives/dropdown';
import Router from 'Modules/Router';

let Tyres = Vue.extend({
  el: '#tyresContext',
  mixins: [VueFilters, VueNavStack],
  directives: {
    'dropdown': DropdownDirective,
  },
  data() {
    return {
      screensSelector: '#tyresScreens',
      searchFor: 'params',
      activeScreen: 'params',
      tyresHtml: $('#tyresHtml').html(),
      jqXHR: null,
      paramsFilters: {
        width: '',
        profile: '',
        diameter: '',
        carManufacturer: '',
        carModel: '',
        carYear: '',
        carType: '',
        season: '',
        brand: '',
      },
      options: {
        width: [],
        profile: [],
        diameter: [],
        carManufacturer: [],
        carModel: [],
        carYear: [],
        carType: [],
        season: [],
        brand: [],
      },
    };
  },
  created() {
    this.$on('loaded', this.onLoaded);
    this.$on('push', this.onPush);
    this.$data.screensSelector = '#tyresScreens';
  },
  updated() {
    $(document).trigger('lazyload');
  },
  methods: {
    onLoaded() {
      $(document).trigger('lazyload');
    },
    onPush(name) {
      this.$data.activeScreen = name;
    },
    initFilters() {

    },
    initParamsFilters() {

    },
    initCarFilters() {

    },
    getData() {
      let data = [];
      switch (this.$data.searchFor) {
        case 'params':
        default:
          return {
            width: this.$data.paramsFilters.width,
            profile: this.$data.paramsFilters.profile,
            diameter: this.$data.paramsFilters.diameter,
            season: this.$data.paramsFilters.season,
            brand: this.$data.paramsFilters.brand,
          };
        case 'car':
          return {
            carManufacturer: this.$data.paramsFilters.carManufacturer,
            carModel: this.$data.paramsFilters.carModel,
            carYear: this.$data.paramsFilters.carYear,
            carType: this.$data.paramsFilters.carType,
            season: this.$data.paramsFilters.season,
            brand: this.$data.paramsFilters.brand,
          };
      }
    },

    filter(showTires = true) {
      this.$data.jqXHR && this.$data.jqXHR.readyState !== 4 && this.$data.jqXHR.abort();
      this.$data.jqXHR = $.ajax({
        url: Router.generate('tyre_index', this.getData(), window.location.host),
        data: this.getData(),
        localCache: false,
        cacheTTL: .1,
        complete: function(jqXHR) {
          //console.log(this);
          //history.replaceState({}, 'Шины\Диски', this.url);
        },
        success: function(data, textStatus, jqXHR) {
          this.$data.tyresHtml = data.results.tyresHtml;
          showTires && parseInt(data.results.tyresCount) > 0 && this.go('tyresList');
          $.each(data.results.filters, function(key, filter) {
            let $dropdown = $('#filter' + key).closest('.dropdown');
            filter.disabled
                ? $dropdown.addClass('disabled')
                : $dropdown.removeClass('disabled');
            let options = [{value: '', text: filter.placeholder, name: filter.placeholder}];
            let current = $dropdown.dropdown('get value');
            $.each(filter.options, function(key, option) {
              options.push({
                value: option.id,
                text: option.name,
                name: option.name,
              });
            }.bind(this));
            $dropdown.dropdown('change values', options).dropdown('set selected', filter.selected,
            );

          }.bind(this));
        }.bind(this),
      });
    },
  },
  watch: {
    searchFor(value) {
      switch (value) {
        case 'params':
        default:
          this.initFilters();
          this.initParamsFilters();
          break;
        case 'car':
          this.initFilters();
          this.initCarFilters();
          break;
      }
    },
    'paramsFilters.width': {
      handler: function(newValue, oldValue) {
        if (newValue !== oldValue) {
          this.filter(false);
        }
      },
    },
    'paramsFilters.profile': {
      handler: function(newValue, oldValue) {
        if (newValue !== oldValue) {
          this.filter(false);
        }
      },
    },
    'paramsFilters.diameter': {
      handler: function(newValue, oldValue) {
        if (newValue !== oldValue) {
          this.filter(false);
        }
      },
    },
    'paramsFilters.carManufacturer': {
      handler: function(newValue, oldValue) {
        if (newValue !== oldValue) {
          this.filter(false);
        }
      },
    },
    'paramsFilters.carModel': {
      handler: function(newValue, oldValue) {
        if (newValue !== oldValue) {
          this.filter(false);
        }
      },
    },
    'paramsFilters.carYear': {
      handler: function(newValue, oldValue) {
        if (newValue !== oldValue) {
          this.filter(false);
        }
      },
    },
    'paramsFilters.carType': {
      handler: function(newValue, oldValue) {
        if (newValue !== oldValue) {
          this.filter(false);
        }
      },
    },
    'paramsFilters.season': {
      handler: function(newValue, oldValue) {
        if (newValue !== oldValue) {
          this.filter(false);
        }
      },
    },
    'paramsFilters.brand': {
      handler: function(newValue, oldValue) {
        if (newValue !== oldValue) {
          this.filter(false);
        }
      },
    },
  },
});

$(function() {
  window.$$.tyres = new Tyres();
});