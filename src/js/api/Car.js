import $ from 'jquery';
import Validator from 'Modules/Validator';
import WatchJs from 'watchjs';
import Router from 'Modules/Router';

class Car {
  constructor(data = {}, options = {}) {
    this.info = $.extend({
      vin: null,
      body: null,
      engine: null,
      manufacturer: 0,
      manufacturerName: null,
      model: 0,
      modelName: null,
      year: null,
    }, data);
    this.options = $.extend({
      dispatcherType: null,
      vueInstance: null,
    }, options);
    this.manufacturer = {};
    this.model = {id: 0, name: ''};
    this.valid = false;
    this.request = null;
    this.eventDispather = {
      unavailable: [],
      available: [],
      model: [],
    };
    this.options.hasOwnProperty('watch') && this.options.watch && this.initWatch();
  }

  initWatch() {
    let watch = WatchJs.watch;
    watch(this.info, 'manufacturer', function(prop, action, newValue, oldValue) {
      this.watchManufacturer(prop, action, newValue, oldValue)
    }.bind(this));
  }

  /**
   * Получаем информацию об автомобиле
   * по Vin номеру
   * @param vin
   * @return Car
   */
  getInfoByVin(vin) {
    Validator.checkVin(vin) ? (function() {
      this.request && this.request.abort();
      this.request = $.ajax({
        url: Router.generate('get_car_by_vin', {vin: vin}, window.location.host),
        dataType: 'json',
        success: function(response) {
          this.info = $.extend(this.info, response.data);
          this.info.vin = vin;
          this.info.body = response.data.body;
          this.info.engine = response.data.engine;
          this.info.manufacturer = response.data.manufacturer;
          this.info.manufacturerName = response.data.manufacturer_name;
          this.info.model = response.data.model;
          this.info.modelName = response.data.model_name;
          this.info.year = response.data.year;
          this.dispatcher('getInfoByWin', this.info);
        }.bind(this),
        error: function() {
          console.warn('During the validation of the VIN / FRAME number, an error occurred');
        }.bind(this),
      });
    }.bind(this))() : console.warn('Vin code is not available!');
    return this.request;
  }

  getModelsForManufacturer(manufacturer) {
    this.request && this.request.abort();
    this.request = $.ajax({
      url: Router.generate('api-get-models-for-manufacturer', {parentEntity: manufacturer}, window.location.host),
      dataType: 'json',
      localCache: true,
      cacheTTL: 96,
      error: function() {
        console.warn('Data not found');
      }.bind(this)
    });
    return this.request;
  }

  getInfo() {
    return this.info;
  }

  setVin(vin) {
    this.info.vin = vin;
    this.getInfoByVin(vin);
    return this;
  }

  checkVin(vin = null) {
    vin && (vin = this.info.vin);
    return Validator.checkVin(vin);
  }

  setManufacturer(brand) {
    return this;
  }

  on(event, callback) {
    this.eventDispather[event].push(callback);
  }

  trigger(event) {
    $.each(this.eventDispather[event], function(key, callback) {
      callback(this);
    }.bind(this));
  }

  reset(field){
    this.info.hasOwnProperty(field) && (this.info[field] = null);
  }

  dispatcher(event, data) {
    this.options.hasOwnProperty('dispatcherType')
    && this.options.dispatcherType === 'vue'
    && this.options.hasOwnProperty('vueInstance')
    && this.options.vueInstance.$emit(event, data);
  }

  /**
   * Заполненна ли вся основная информацию об автомобиле
   * @return {boolean}
   */
  checkCar(strict = false) {
    return (!strict || (typeof this.info.body === 'string' && this.info.body.length))
        && (!strict || (typeof this.info.engine === 'string' && this.info.engine.length))
        && ((typeof this.info.year === 'string' && parseInt(this.info.year) > 1970) ||
            (typeof this.info.year === 'number' && this.info.year > 1970))
        && ((typeof this.info.model === 'string' && this.info.model.length > 0) ||
            (typeof this.info.model === 'number' && typeof this.info.model > 0))
        && ((typeof this.info.manufacturer === 'string' && this.info.manufacturer.length > 0) ||
            (typeof this.info.manufacturer === 'number' && this.info.manufacturer > 0));
  }

  /**
   * Следим за именением бренда производителя,
   * и если он сменился то подгружаем новые модели
   * @param prop
   * @param action
   * @param newValue
   * @param oldValue
   */
  watchManufacturer(prop, action, newValue, oldValue) {
    newValue !== oldValue
    && this.getModelsForManufacturer(newValue).done(function(response) {
      this.manufacturer.models = response;
      this.dispatcher('getModelsByManufacturer', response);
    }.bind(this));
  }
}

export default Car;