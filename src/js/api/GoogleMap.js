import $ from 'jquery';
import 'Semantic/modal';
import CallbackStack from 'Modules/CallbackStack';

class GoogleMap {
  /**
   * Подключение и управление Google картами
   */
  constructor(API_KEY, callback = null) {
    this.data = {
      map: null,
      markers: [],
    };
    this.callbackStack = new CallbackStack('googleMapReady');
    if ($('#GoogleMapScript').length < 1) {
      this.script = document.createElement('script');
      this.script.async = true;
      this.script.id = 'GoogleMapScript';
      this.script.src = 'https://maps.googleapis.com/maps/api/js?key=' +
        API_KEY + '&callback=googleMapReady';
      document.body.appendChild(this.script);
    }
    typeof callback === 'function' && this.callbackStack.add(callback);
    window.googleMapReady = function() {
      this.callbackStack.perform('googleMapReady');
    }.bind(this);
  }

  /**
   * Инициализируем карту
   * @param id
   * @param center
   * @param zoom
   * @return {null}
   */
  map(id, center, zoom) {
    this.data.map = new google.maps.Map(document.getElementById(id), {
      zoom: zoom,
      center: center,
    });
    return this.data.map;
  }

  /**
   * Добавить маркер на карту
   * @param name
   * @param position
   * @param options
   * @return {GoogleMap}
   */
  marker(name, position, options = {}) {
    options = $.extend({
      position: position,
      map: this.data.map,
      title: '',
    }, options);
    this.data.markers[name] = new google.maps.Marker(options);
    return this;
  }

  modal(id, center, zoom) {
    let template = `<div class="ui modal" id="mapModal-` + id + `">
                    <div class="header">Header</div>
                    <div class="content">
                      <div class="ui 4:3 embed" id="` + id + `"></div>
                    </div>
                    <div class="actions">
                      <div class="ui approve button">Approve</div>
                      <div class="ui button">Neutral</div>
                      <div class="ui cancel button">Cancel</div>
                    </div>
                  </div>`;
    $('.ui.basic.modal')
    .modal('show');
    // $.fancybox.open(template,
    //   {
    //     afterShow: function() {
    //       return this.map(id, center, zoom);
    //     }.bind(this),
    //   });
  }

}

export default GoogleMap;
