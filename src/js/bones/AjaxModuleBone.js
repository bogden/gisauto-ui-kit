import BaseBone from './BaseBone';

class AjaxModuleBone extends BaseBone {
  constructor() {
    super();
    this.resultStack = [];
  }
}

export default AjaxModuleBone;