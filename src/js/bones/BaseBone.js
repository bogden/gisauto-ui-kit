import $ from 'jquery';

class BaseBone {
  constructor() {
    this.params = {};
    !window.hasOwnProperty('$$') && (window.$$ = {});
  }

  init() {
    return this;
  }

  /**
   * Задать параметры модуля
   * @param {object} params - Объект параметров
   * @param {bool} deep - "Глубокая" склейка параметров
   * @return {BaseBone}
   */
  setParams(params, deep = false) {
    $.extend(deep, this.params, params);
    return this;
  }

  /**
   * Получить параметры модуля
   * @return {{}|*}
   */
  getParams() {
    return this.params;
  }

  /**
   * Возвращает указанное значение если первый параметр false или undefined
   * @param value
   * @param defaultValue
   * @param isUndefined
   * @return {null}
   */
  static onDefault(value, defaultValue, isUndefined = false) {
    if (!isUndefined) {
      return !arguments[0] ?
        (arguments[1] ? arguments[1] : null) :
        arguments[0];
    } else {
      return typeof arguments[0] === 'undefined' ?
        (arguments[1] ? arguments[1] : null) :
        arguments[0];
    }
  };

  destruct() {
    delete this;
    return null;
  }
}

export default BaseBone;
