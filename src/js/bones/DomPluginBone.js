import BaseBone from './BaseBone';
import $ from 'jquery';

/**
 * Кости для модуля работы с DOM деревом
 */
class DomPluginBone extends BaseBone {
  constructor() {
    super();
    this.selector = null;
    this.element = null;
    this.$element = null;
    this.params = {};
    this.manipulator = 'jQuery';
  }

  /**
   * Установка метода выбора едемента дерева.
   * @param manipulator
   */
  setManipulator(manipulator) {
    this.manipulator = manipulator;
  }

  /**
   * Инициализация работы модуля и начало манипуляции над DOM.
   * @param {string|object|array} select
   * @param {object|array} params
   * @return DomPluginBone
   */
  init(select, params = null) {
    switch (arguments.length) {
      case 0:
        return this;
      case 1:
        this.select(...arguments);
        break;
      default:
        switch (typeof arguments[0]) {
          case 'object':
          case 'array':
            this.select(...arguments[0]);
            break;
          default:
            this.select(arguments[0]);
            break;
        }
        switch (typeof arguments[1]) {
          case 'object':
          case 'array':
            this.setParams(BaseBone.onDefault(arguments[1], {}));
            break;
        }
    }
    return this;
  }

  /**
   * Выбирает заданный элемент DOM дерева
   * с указанными аргументами и с помощью заданного манипулятора.
   */
  select() {
    switch (this.manipulator) {
      case 'native':
        this.element = document.querySelectorAll(...arguments);
        break;
      case 'jQuery':
      case '$':
      default:
        this.element = $(...arguments);
        break;
    }
  }

  getElement() {
    return this.element;
  }

  getElements() {
    return this.element;
  }
}

export default DomPluginBone;