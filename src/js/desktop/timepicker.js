import $ from 'jquery';
import moment from 'moment';
import Dragdealer from 'dragdealer';

class Timepicker {
  constructor(selector) {
    this.$document = $(document);
    this.selector = selector;
    this.$element = $(selector);
    this.drag = null;
    this.data = {
      created: false,
      startDate: '',
      endDate: '',
      draggerPos: '',
      timelinePos: '',
      selectedLine: '',
      years: [],
      months: [],
      now: moment(),
      startMonth: moment().add(-5, 'years'),
    };
  }

  /**
   * @param {str} selector Element ID
   * @param step
   * @param steps
   */
  initDrag(selector, step = 60, steps = 60) {
    this.drag = new Dragdealer(selector, {
      x: 1,
      steps: steps,
      callback: this.generateCalendars.bind(this),
    });
    //console.log('startMonth: ', this.data.startMonth.day());
  }

  generateCalendars() {
    let step = this.drag.getStep();//
    let startMonth = moment().add(-5, 'years');//

    //let currentMoment = startMonth.add(step[0], 'months');
    //let weekDay = currentMoment.startOf('month').day();
    //let daysAll = currentMoment.endOf('month').format('DD') || 0;
    //let calendarTable = '';
    //let monthDays = 1;
    //let countTD = 0;

    for (let i = 0; i < 3; i++) {
      let nextStep = step[0] + i;
      let calendar = this.createCalendar(nextStep);

      $('#time1').find('.timepick__month_flex' + (i + 1)).empty().append(calendar);
    }
  }

  createCalendar(step) {

    //console.log(currentMoment.format('DD-MM-YYYY'));
    console.log(step);
    let currentMoment = moment().add(-5, 'years').add(step, 'months');
    console.log(currentMoment.calendar());

    let weekDay = currentMoment.startOf('month').day() + 1;
    let daysAll = currentMoment.endOf('month').format('DD');
    let calendarTable = '';
    let monthDays = 1;
    let countTD = 0;


    for (let i = 0; i < weekDay; i++) {
      calendarTable += '<div class="qwe"></div>';
      countTD++;
    }
    for (let i = 0; i < daysAll; i++) {
      calendarTable += '<div>' + monthDays + '</div>';
      countTD++;
      monthDays++;
    }
    for (let i = countTD; i < 35; i++) {
      calendarTable += '<div class="qwe"></div>';
      countTD++;
    }
    return calendarTable;
  }

  getMonths(month, year) {
    var ar = [];
    var start = moment(year + '-' + month, 'YYYY-MMM');
    for (var end = moment(start).add(1, 'month');
         start.isBefore(end); start.add(1, 'day')) {
      ar.push(start.format('D-ddd'));
    }
    return ar;
  }

  init() {
    return this;
  }
}

let timepicker = (new Timepicker('#timepick__timeline')).init().
    initDrag('timepick__timeline');
