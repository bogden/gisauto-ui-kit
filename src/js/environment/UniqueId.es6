class UniqueId {
  /**
   * Добавляет уникальный ID каждому объекту
   * @return {number}
   */
  constructor(){

    (function() {
      let id_counter = 1;
      Object.defineProperty(Object.prototype, "__uniqueId", {
        writable: true
      });
      Object.defineProperty(Object.prototype, "uniqueId", {
        get: function() {
          if (this.__uniqueId === undefined)
            this.__uniqueId = id_counter++;
          return this.__uniqueId;
        }
      });
    }());
  }

  getCounter(){
    return this.counter;
  }

  static load(){
    window.objectCounter = new UniqueId();
  }
}

export default UniqueId;