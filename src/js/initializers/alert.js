import $ from 'jquery';
import Alert from 'Modules/Alert';

$(function() {
  if (!window.fn) {
    window.fn = {};
  }
  if (!window.$$) {
    window.$$ = {};
  }
  window.fn.alert = window.$$.alert = new Alert();
});
