import $ from 'jquery';
import NavStack from 'Modules/NavStack';

$(function() {
  if (!window.$$) {
    window.$$ = {};
  }
  window.$$.navStack = new NavStack('#stack');

  // window.$$.navStack
  //   .add('test', '<h1>lorem</h1>')
  //   .add('test2', '<h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusan</h1>')
  //   .push('test2');
  // setTimeout(function() {
  //   window.$$.navStack.push('test');
  //   console.log('test');
  // }, 3000);
  // setTimeout(function() {
  //   window.$$.navStack.push('test');
  //   console.log('test2');
  // }, 6000);
  //
  // console.log(window.$$.navStack.getScreens());
});
