import $ from 'jquery';
import Form from 'Modules/Form';

$(function() {
  let form = new Form('.example-form-ajax', {});
  form.initForm();
});
