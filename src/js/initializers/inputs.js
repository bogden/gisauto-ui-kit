import $ from 'jquery';
import Input from 'Modules/Input';

$(function() {
  if (!window.$$) {
    window.$$ = {};
  }
  window.$$.input = new Input();

  window.$$.input
  .initNumbersInput('input[type="number"]')
  .eraseControl()
  .passwordControl('.form-control-password')
  .phoneControl('.form-control-phone')
  .emailControl('.form-control-email');
  Input.fixtures();
});
