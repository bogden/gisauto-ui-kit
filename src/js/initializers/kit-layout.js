import $ from 'jquery';
import 'Semantic/popup';
import Markdown from 'showdown';
import Example from 'Modules/example/Example';
import Hightlight from 'highlight.js';
import 'Semantic/sticky';

$(function() {
  let showdown = new Markdown.Converter();
  $('.showdown').each(function() {
    let $this = $(this);
    $this.html(showdown.makeHtml($this.text().trim()));
    $this.find('pre code').each(function() {
      Hightlight.highlightBlock(this);
    });
  });

  /**
   * Ликое меню
   */
  $('#navbar').sticky({
    context: '#main',
    pushing: true,
    offset: 8
  })
  ;

  $('.browse.item').popup({
    on: 'click',
    popup: '.popup-menu',
    target: '.browse.item',
  });
  $('.api.item').popup({
    on: 'click',
    popup: '.popup-api-menu',
    target: '.api.item',
  });
  Example.examples('.example');

});
