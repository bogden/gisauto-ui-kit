import $ from 'jquery';
import Bar from 'Modules/mobile/Bar';

$(function() {
  if (!window.$$) {
    window.$$ = {};
  }
  window.$$.bar = new Bar('#siteHeader');
  window.$$.bar.initialize();
});
