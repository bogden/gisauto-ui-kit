import $ from 'jquery';
import Sidebar from 'Modules/mobile/Sidebar';

$(function() {
  if (!window.$$) {
    window.$$ = {};
  }
  window.$$.sidebar = new Sidebar('#sidebar', '#sidebarSwiper');
  window.$$.sidebar.initialize().listeners();

});
