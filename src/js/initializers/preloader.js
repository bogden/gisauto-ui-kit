import $ from 'jquery';
import Preloader from 'Modules/Preloader';

$(function() {
  if (!window.$$) {
    window.$$ = {};
  }
  window.$$.preload = new Preloader('#preloadExample', {
    theme: 'dark',
  }).show().
      control($('#preloaderShow'), 'click', 'show').
      control($('#preloaderHide'), 'click', 'hide');
  let preloadBody = new Preloader('body', {
    theme: 'light',
  }).control($('#preloaderBodyShow'), 'click', 'show').
      control($('#preloaderBodyHide'), 'click', 'hide');
});
