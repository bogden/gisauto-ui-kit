import $ from 'jquery';
import Rating from 'Modules/Rating';

$(function() {
  if (!window.$$) {
    window.$$ = {};
  }
  window.$$.rating = new Rating('.ui.rating', {
    interactive: true,
  });
  window.$$.rating.init();
});
