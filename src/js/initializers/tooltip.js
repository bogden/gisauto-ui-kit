import $ from 'jquery';
import 'Semantic/popup';
import 'Semantic/transition';


$(function () {
    $('.ui-tooltip .ui-tooltip-toggle')
        .popup({
            inline: true,
            on: 'click',
            position: 'bottom right',
            delay: {
                show: 300,
                hide: 500
            }
        });
    $('.js-call-tooltip')
        .popup({
            inline: true,
            on: 'click',
            position: 'bottom right',
            setFluidWidth: false,
            delay: {
                show: 300,
                hide: 500
            }
        });
});
