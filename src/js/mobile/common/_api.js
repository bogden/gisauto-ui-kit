import $ from 'jquery';

/**
 * Работа со списками городов
 * позвонляет работать с городами и геометками
 */
module.exports = class apiCities {
  constructor() {
    this.api = Routing.generate('mobile_cities_ajax');
    this.xhr = null;
    this.data = [];
  }

  /**
   * Получить список городов по заданному параметру
   * @param query
   * @return {Array}
   */
  get(query) {
    // new Promise(function (resolve) {
    //
    // }).then(function (data) {
    //
    // });
    if (this.xhr) this.xhr.abort();
    this.xhr = $.ajax({
      url: this.api,
      dataType: 'json',
      async: false,
      cache: true,
      data: {searchString: query},
      success: function(response) {
        this.data = response.data;
      }.bind(this)
    });
    return this.data;
  }
};
