import $ from 'jquery';

function commonFunctions() {
  $(function() {
    if (!window.$$) {
      window.$$ = {}
    }
    /**
     * Склонение слов с числительными
     * @param {number} number Колличество чего либо.
     * @param {Array} titles Массив из треъ елементов с вариациями слов .
     * @return {string}
     */
    window.transChoose = function(number, titles) {
      let cases = [2, 0, 1, 1, 1, 2];
      return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
    };

    window.$$.getJSON = function(url, data, callback) {
      let defaultCalback = {
        success: function(data) {
        },
        error: function(data) {
        }
      };
      callback = $.extend(defaultCalback, callback);
      $.ajax({
        url: url,
        dataType: 'json',
        success: callback.success,
        error: callback.error
      });
    };
  });
}

export default commonFunctions;