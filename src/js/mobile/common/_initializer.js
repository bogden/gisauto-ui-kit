$(function() {
  if(!window.$$){
    window.$$ = {};
  }

  window.$$.common = new Common();
  window.$$.semantic = new Semantic();
  window.fn = new Functions();
  window.$$.actions = new Action();
  window.$$.sidebar = new Sidebar('#sidebar', '#sidebarSwiper');
  window.$$.bar = new Bar('#siteHeader');

  window.$$.bar.initialize();
  window.$$.actions.back('[data-go-back]', 'click'); // Кнопка назад
  window.$$.sidebar.initialize().listeners();
  window.$$.common.init();

  window.$$.initStack = [];
  window.$$.initInput = function() {
    window.$$.inputManipulator = new InputManipulators();
    window.$$.inputManipulator
      .initNumbersInput('input[type="number"]')
      .eraseControl('.form-control-erase')
      .passwordControl('.form-control-password')
      .phoneControl('.form-control-phone')
      .emailControl('.form-control-email')
      .searchControl('.form-control-search');
    /**
     * Фикстуры для инпутов, прендазначенные для коректного
     * отображения некоторых кастомных елементов.
     */
    InputManipulators.fixtures();
  };

  // Инициализация "ленивой" загрузки изображений
  window.$$.initLazyload = function() {
    window.$$.lazyInstance = $('img.lazy').Lazy({
      afterLoad: function(element) {
        let imageSrc = element.data('src');
        $(element).addClass('lazy-loaded').siblings('.lazy-loader').fadeOut();
      },
      onError: function(element) {
        let imageSrc = element.data('src');
      }
    });
    return window.$$.lazyInstance;
  };

  $(document)
  /**
   * Инициализация инпутов и привязка событий
   * Для переинициализации инпутов на странице достаточно вызвать
   * событие 'init.inputs' на document
   */
    .on('init init.inputs', function() {
      window.$$.initInput();
      $$.initStack.push('init.inputs');
    })
    .trigger('init.inputs')
    /**
     * Инициализация ленивой загрузки и привязка событий
     * Для переинициализации "Ленивой загрузки" на странице достаточно вызвать
     * событие 'init.lazy' на document
     */
    .on('init init.lazy', function() {
      window.$$.initLazyload();
      $$.initStack.push('init.lazy');
    })
    .trigger('init.lazy');
});
