import $ from 'jquery';
import Action from 'Modules/Action';

$(function() {
  if (!window.$$) {
    window.$$ = {};
  }
  window.$$.action = new Action();
  window.$$.action.back('[data-go-back]', 'click'); // Кнопка назад
});
