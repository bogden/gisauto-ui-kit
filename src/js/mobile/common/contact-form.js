import $ from 'jquery';
import '@fancyapps/fancybox';
import 'owl.carousel';
import Form from 'Modules/Form';
import Dropdown from 'Modules/Dropdown';
import Modal from 'Modules/Modal';
import 'semantic-ui/dist/components/api';


module.exports = $(function() {
  let form = null;
  /**
   * При кликена элемент открываем модальное окно которое по ajax получает тело формы.
   */
  $().fancybox({
    selector: '[data-toggle="contact-form"]',
    buttons: ['close'],
    animationEffect: 'zoom',
    animationDuration: 200,
    zoomOpacity: 'auto',
    transitionEffect: 'zoom-in-out',
    lang: 'ru',
    ajax: {
      settings: {
        data: {
          fancybox: true,
        },
      },
    },
    afterShow: function() {
      form = new Form('.form-contact', {
        finder: {
          prefix: 'contact[',
          postfix: ']',
        },
      });
      form.initForm();
      $('#contactFormCarousel').owlCarousel({
        items: 1,
        margin: 10,
        autoHeight: true,
        loop: false,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
      });
      (new Dropdown('.ui.dropdown.city-dropdown', {}, form.getForm())).initCityDropdown('.ui.dropdown.city-dropdown');
      (new Dropdown('.ui.dropdown.recipient-dropdown', {
        onChange: Dropdown.onChange,
      }, form.getForm())).init('.ui.dropdown.recipient-dropdown');
    },

    i18n: {
      'ru': {
        CLOSE: 'Закрыть',
        NEXT: 'Далее',
        PREV: 'Назад',
        ERROR: 'The requested content cannot be loaded. <br/> Please try again later.',
        PLAY_START: 'Запустить слайдшоу',
        PLAY_STOP: 'Пауза',
        FULL_SCREEN: 'На весь экран',
        THUMBS: 'Миниатюры',
        DOWNLOAD: 'Загрузить',
        SHARE: 'Поделится',
        ZOOM: 'Увеличить',
      },
    },
  });
  $(document).on('submitted.contact', function(e, data) {

  });
});
