import $ from 'jquery';
import Rating from 'Modules/Rating';

$(function() {
  if (!window.$$) {
    window.$$ = {};
  }
  window.$$.rating.initView('.ui.rating', {
    interactive: true,
  });
});
