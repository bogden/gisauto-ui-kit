import $ from 'jquery';
import TextCollapse from 'Modules/TextCollapse';

$(function() {
  TextCollapse.initPlugin();
  $('.text-collapse').textCollapse({
    revealText: 'Показать ещё...',
    hideText: 'Свернуть',
    linesNumber: 2,
    hideMore: false,
    dots: false,
  });
});
