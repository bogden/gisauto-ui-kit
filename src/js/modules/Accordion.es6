import $ from 'jquery';
import 'Semantic/accordion';

/**
 * Интерфейс работы с "Аккордионами"
 *
 * - *Declare globally
 * - *Context
 * - *Init
 * - *Apply
 */
class Accordion{
  constructor(params = {}){
    this.params = params;
    this.$context = $(document);
    this.$elements = $('');
  }

  /**
   * Инициализация плагина
   * @param selector
   * @param options
   * @return {Accordion}
   */
  init(selector, options = {}){
    this.selector = selector;
    this.$elements = $(selector, this.$context);
    this.$elements.accordion(options);
    return this;
  }

  /**
   * Применить действие или настройки
   * @param options
   * @param param
   * @return {Accordion}
   */
  apply(options = {}, param = undefined){
    this.$elements.accordion(options, param);
    return this;
  }

  setContext($context = null){
    this.$context = $context ? $context : $(document);
    return this;
  }

  declareGlobally(moduleName){
    !window.hasOwnProperty('$$') && (window.$$ = {});
    window.$$[moduleName] = this;
    return this;
  }

  /**
   * Refreshes all cached selectors and data
   * @return {Accordion}
   */
  refresh(){
    this.apply('refresh');
    return this;
  }

  /**
   * Opens accordion content at index
   * @param index
   * @return {Accordion}
   */
  open (index){
    this.apply('open', index);
    return this;
  }

  /**
   * Closes accordion content that are not active
   * @return {Accordion}
   */
  closeOthers(){
    this.apply('close other');
    return this;
  }

  /**
   * Closes accordion content at index
   * @param index
   * @return {Accordion}
   */
  close (index){
    this.apply('close', index);
    return this;
  }

  /**
   * Toggles accordion content at index
   * @param index
   * @return {Accordion}
   */
  toggle (index){
    this.apply('toggle', index);
    return this;
  }

}

export default Accordion;