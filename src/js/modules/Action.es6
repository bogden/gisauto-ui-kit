import $ from 'jquery';

class Action {
  constructor() {
    this.$body = $('body');
  }

  /**
   * Назад по истории браузера
   * @param selector
   * @param eventName
   * @returns {Action}
   */
  back(selector, eventName = 'click') {
    this.$body.on(eventName, selector, function(e) {
      e.preventDefault();
      if (history.length === 1) {
        window.location = '/';
      } else {
        history.back();
      }
    });
    return this;
  }

  /**
   * Назад по истории браузера
   * @param selector
   * @param eventName
   * @returns {Action}
   */
  forward(selector, eventName = 'click') {
    this.$body.on(eventName, selector, function(e) {
      e.preventDefault();
      history.forward();
    });
    return this;
  }
}

export default Action;

