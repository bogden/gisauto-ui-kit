import DomPluginBone from '../bones/DomPluginBone';
import CallbackStack from 'Modules/CallbackStack';
import $ from 'jquery';

class ActionManager extends DomPluginBone {
  constructor() {
    super();
    this._callbackStack = new CallbackStack();
    this.setManipulator('jQuery');
    window.perform = function() {
      this.perform(...arguments);
    }.bind(this);
  }

  initTriggers() {
    this.getElements().
        on('click', '[data-click]', function(e) {
          let $this = $(e.currentTarget);
          $this.data('prevent') && (e.preventDefault());
          this._callbackStack.perform($this.data('click'));
        }.bind(this)).
        on('hover', '[data-hover]', function(e) {
          let $this = $(e.currentTarget);
          $this.data('prevent') && (e.preventDefault());
          this._callbackStack.perform($this.data('hover'));
        }.bind(this));
        $(document).ready(function() {
          $('[data-ready]').each(function(index, item) {
            this._callbackStack.perform($(item).data('ready'));
          }.bind(this));
        }.bind(this));
    return this;
  }

  addCommand(name = '', callback = null) {
    this._callbackStack.add(...arguments);
    return this;
  }

  perform(command = '') {
    this._callbackStack.perform(command);
  }

  on(event, target, command) {
    this.getElements().on(event, target, function() {
      this._callbackStack.perform(command);
    }.bind(this));
    return this;
  }

  static load() {
    !window.hasOwnProperty('$$') && (window.$$ = {});
    !window.$$.hasOwnProperty('ActionManager') && (window.$$.ActionManager = new ActionManager());
    return window.$$.ActionManager;
  }
}

export default ActionManager;