import $ from 'jquery';
import Modal from 'Modules/Modal';
import LocationSelect from './LocationSelect';

class Alert {
  constructor() {
    this.alert = Alert.alert;
    (function(proxied) {
      window.classicAlert = proxied;
      window.alert = function() {
        Alert.alert(arguments[0]);
        return null;
      };
    })(window.alert);
  }

  static alert(text = '', title = null) {
    Modal.openStatic('alert', '<p>' + text + '</p>', {
      classes: 'dark alert size mini',
      header: !!title,
      title: !!title ? title : null,
      footer: true,
      footerContent: '<button data-fancybox-close class="ui button pink circular">Окей</button>'
    },{
      buttons: []
    })
  }

  static load(){
    !window.hasOwnProperty('$$') && (window.$$ = {});
    !window.$$.hasOwnProperty('Alert') && (window.$$.Alert = new Alert());
    return window.$$.Alert;
  }
}

export default Alert;