import $ from 'jquery';
import AjaxModuleBone from '../bones/AjaxModuleBone';
import Router from 'Modules/Router';
import Modal from 'Modules/Modal';
import Form from 'Modules/Form';
import '@fancyapps/fancybox';

class Authorizer extends AjaxModuleBone{
  constructor() {
    super();
    this.modal = new Modal();
    this.modal.initActionManager();
  }

  authorizeModal(){
    if(!this.waiting){
      this.modal.openAjax('authorizeModal', Router.generate('users-authorization', {}, window.location.host), {}, {
        header: true,
        title: 'Войти',
      }, {
        afterShow: function() {
          this.form = (new Form('#formAuthorization', {})).initForm().onSubmitted(function() {
            alert('Авторизация прошла успешно!');
            window.location.reload();
          });
          return this;
        }.bind(this)
      });
      return this;
    }
  }
}

export default Authorizer;