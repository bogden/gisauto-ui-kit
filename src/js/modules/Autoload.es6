import $ from 'jquery';
import CallbackStack from './CallbackStack';

class Autoload {
  constructor() {
    this.ready = new CallbackStack('documentReady');
    this.loaded = new CallbackStack('windowLoad');
    document.addEventListener("DOMContentLoaded", function(){
      this.ready.perform('documentReady')
    }.bind(this));
    window.onload = function(){
      this.ready.perform('windowLoad')
    }.bind(this)
  }

  onLoaded(callback) {
    this.loaded.add('windowLoad', callback);
    return this;
  }

  onReady(callback){
    this.ready.add('documentReady', callback);
    return this;
  }

  static load(){
    !window.hasOwnProperty('$$') && (window.$$ = {});
    !window.$$.hasOwnProperty('Autoload') && (window.$$.Autoload = new Autoload());
    return window.$$.Autoload;
  }
}

export default Autoload;