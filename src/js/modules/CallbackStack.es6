class CallbackStack {
  constructor(name = null) {
    !window.hasOwnProperty('$$') && (window.$$ = {});
    !window.$$.hasOwnProperty('callbackStack') && (window.$$.callbackStack = []);
    name && this.create(name);
  }

  /**
   * Создать стек
   * @param {string} name
   */
  create(name = '') {
    if(typeof name !== 'string'){
      console.warn('Stack name must be string');
      return this;
    }
    this.activeStack = name;
    !window.$$.callbackStack.hasOwnProperty(name) && (window.$$.callbackStack[name] = []);
    return this;
  }

  /**
   * Добавить в стек обратных вызовов
   * @param name
   * @param callback
   */
  add(name = '', callback = null) {
    if (arguments.length  === 1) {
      typeof arguments[0] === 'function'
      && (window.$$.callbackStack[this.activeStack].push(arguments[0]));
    } else {
      this.create(arguments[0]);
      typeof arguments[1] === 'function'
      && (window.$$.callbackStack[arguments[0]].push(arguments[1]));
    }
    return this;
  }

  /**
   * Выполнить стек
   * @param name
   * @param erase
   */
  perform(name = null, erase = false) {
    !name && (name = this.activeStack);
    if(window.$$.callbackStack.hasOwnProperty(name)){
      window.$$.callbackStack[name].forEach(function(callback, index) {
        callback();
      });
      erase && (window.$$.callbackStack[name] = []);
    }else{
      console.warn('Callback stack with name: ' + name + ' not found!');
    }

    return this;
  }
}

export default CallbackStack;