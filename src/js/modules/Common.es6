import $ from 'jquery';

class Common {
  constructor() {
    if (!window.hasOwnProperty('$$')) {
      window.$$ = {};
    }
    window.remSize = Number(getComputedStyle(document.body, null).fontSize.replace(/[^\d]/g, '')) || 16;
    window.rem = function(value, postfix) {
      return value / window.remSize + (postfix ? 'rem' : '');
    };

    window._elements = {
      $document: $(document),
      $body: $('body')
    };
  }

  static objectsIdentifier(){
    var id_counter = 1;
    Object.defineProperty(Object.prototype, "__uniqueId", {
      writable: true
    });
    Object.defineProperty(Object.prototype, "uniqueId", {
      get: function() {
        if (this.__uniqueId == undefined)
          this.__uniqueId = id_counter++;
        return this.__uniqueId;
      }
    });
  }

  static overide() {
    (function(proxied) {
      window.alert = function() {
        $$.alert(arguments[0]);
        return null;
      };
    })(window.alert);
  }

  static modals() {

  }

  static inputs() {
    if (typeof $.fn.inputmask !== 'undefined') {
      $(':input').inputmask();
    }
  }

  static tooltip() {
    if (typeof $.fn.tooltip !== 'undefined') {
      $('[data-toggle="tooltip"]').tooltip();
    }
  }

  /**
   * Инициализация основных плагинов и модулей
   */
  init() {
    Common.overide();
    Common.inputs();
    Common.modals();
    Common.tooltip();
    $(document).on('init.refresh init', function(e) {
      Common.inputs();
      Common.modals();
      Common.tooltip();
    });
    return this;
  }
}

