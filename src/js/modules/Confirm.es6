import $ from 'jquery';

class Confirmer {
  /**
   *
   * @param id
   * @param text
   * @param callback
   * @constructor
   */
  constructor(id, text, callback) {
    this.id = id;
    this.text = text;
    this.$template = $(`
            <div id="` + id + `" class="modal_fancybox-construct modal-tooltip" tabindex="3" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body text-center">
                            <p class="notice-text">` + this.text + `</p>
                            <div class="btn-row justify-content-center">
                                <button class="btn btn-mr btn-brand_pink btn-circle confirmer-confirm">Да</button>
                                <button class="btn btn-mr btn-brand_light btn-circle confirmer-dismiss">Нет</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`);
    this.callback = $.extend({
      confirm: function() {

      },
      dismiss: function() {

      }
    }, callback);
  }

  /**
   * Open confirm window
   * @returns {Confirmer}
   */
  open() {
    this.$template
      .on('click', '.confirmer-confirm', function() {
        this.confirmer.close();
        this.callback.confirm();
      }.bind(this))
      .on('click', '.confirmer-dismiss', function() {
        this.confirmer.close();
        this.callback.dismiss();
      }.bind(this));
    this.confirmer = $.fancybox.open({
      src: this.$template,
      type: 'inline',
      clickSlide: false,
      clickOutside: false,
      opts: {
        afterShow: function(instance, current) {
        }
      }
    });
    return this;
  }

  /**
   * Close confirm window
   * @returns {Confirmer}
   */
  close() {
    this.confirmer.close();
    return this;
  }
}

export default Confirmer;
