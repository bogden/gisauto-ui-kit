import $ from 'jquery';
import 'Semantic/dropdown';
import 'Semantic/transition';
import 'SemanticBehaviors/api';
import Router from 'Modules/Router';
import Feedback from './Feedback';

class Dropdown {
  /**
   * Интерфейс для работы с Dropdown лементами
   * @param selector
   * @param options
   * @param $context
   */
  constructor(selector = null, options = {}, $context = null) {
    Dropdown.translate();
    this.options = {};
    selector && this.setElement(selector, $context).setOptions(options);
  }

  setElement(selector, $context = null) {
    this.$elements = $context
        ? $(selector, $context)
        : $(selector);
    return this;
  }

  setOptions(options) {
    $.extend(this.options, options);
    return this;
  }

  /**
   * Инициализация плагина
   * @param selector
   * @param options
   * @param $context
   * @return {Dropdown}
   */
  init(selector, options = null, $context = null) {
    typeof options === 'object' && this.setOptions(options);
    this.setElement(selector, $context);
    this.$elements.dropdown(this.options);
    return this;
  }

  initCityDropdown(selector, $context = null) {
    $(selector).dropdown({
      fields: {name: 'name', value: 'id'},
      apiSettings: {
        url: decodeURI(Router.generate('api-get-cities-by-query', {query: '{query}'}, window.location.host)),
      },
      placeholder: 'Выберите город'
    });

    return this;
  }

  apply(options = {}, attrPrimary = null, attrSecondary = null) {
    attrPrimary && attrSecondary && this.$elements.dropdown(options, attrPrimary, attrSecondary)
    || attrPrimary && this.$elements.dropdown(options, attrPrimary)
    || this.$elements.dropdown(options);
    return this;
  }

  /**
   * Разрешение конфликтов
   */
  static noConflict() {
    if (typeof $.fn.dropdown !== 'undefined') {
      $.fn.BSDropdown = $.fn.dropdown.noConflict();
    }
  }

  /**
   * Перевод плагина
   */
  static translate() {
    /**
     * Настройки для Dropdown
     * @type {{addResult: string, count: string, maxSelections: string, noResults: string, serverError: string}}
     */
    if ($.fn.hasOwnProperty('dropdown')) {
      $.fn.dropdown.settings.message = {
        addResult: 'Добавить <b>{term}</b>',
        count: '{count} выбрано',
        maxSelections: 'Максимальное количество {maxCount}.',
        noResults: 'Ничего не найдено.',
        serverError: 'Ошибка на сервере.',
      };
    }
  }

  static behavior(element, behavior = '', argumentOne = null, argumentTwo = null){
    $(element).dropdown(behavior, argumentOne, argumentTwo)
  }

  /**
   * Действие при выборе элементов
   * - Добавление кнопки удалить все выбранные пункты
   * @param value
   * @param text
   * @param $choice
   */
  static onChange(value, text, $choice) {
    let $this = $(this);
    value && value.length ?
        (function() {
          $this.addClass('selected');
          $this.siblings('.icon').
              removeClass('dropdown').
              addClass('delete').
              on('click', function() {
                $this.dropdown('set selected', '');
              });
        }()) :
        (function() {
          $this.removeClass('selected');
          $this.siblings('.icon').
              addClass('dropdown').
              removeClass('delete').
              off('click');
        }());
  }

  static load(){
    !window.hasOwnProperty('$$') && (window.$$ = {});
    !window.$$.hasOwnProperty('Dropdown') && (window.$$.Dropdown = new Dropdown());
    return window.$$.Dropdown;
  }
}

export default Dropdown;
