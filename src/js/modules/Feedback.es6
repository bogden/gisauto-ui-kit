import AjaxModuleBone from '../bones/AjaxModuleBone';
import Router from 'Modules/Router';
import Form from 'Modules/Form';
import Dropdown from 'Modules/Dropdown';
import Modal from './Modal';

class Feedback extends AjaxModuleBone {

  constructor() {
    super();
    this.form = null;
  }

  initForm() {
    this.form = new Form('.ui.form.feedback', {
      finder: {
        prefix: 'contact[',
        postfix: ']',
      },
    });
    this.form.initForm();
    (new Dropdown('.ui.dropdown.city-dropdown', {}, this.form.getForm())).initCityDropdown(
        '.ui.dropdown.city-dropdown');
    (new Dropdown('.ui.dropdown.recipient-dropdown', {
      onChange: Dropdown.onChange,
    }, this.form.getForm())).init('.ui.dropdown.recipient-dropdown');
  }

  openFeedbackModal() {
    Modal.openAjaxStatic('feedback', Router.generate('feedback', {}, window.location.host), {type: 'GET'}, {
      header: true,
      title: 'Обратная связь',
    }, {
      afterShow: function(instance) {
        this.initForm();
        console.log(instance);
      }.bind(this),
    });
  }

  static load() {
    !window.hasOwnProperty('$$') && (window.$$ = {});
    !window.$$.hasOwnProperty('Feedback') && (window.$$.Feedback = new Feedback());
    return window.$$.Feedback;
  }
}

export default Feedback;