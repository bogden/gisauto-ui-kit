import $ from 'jquery';
import 'jquery-form/src/jquery.form';
import Inputmask from 'inputmask';
import CallbackStack from 'Modules/CallbackStack';


class Form {
  constructor(selector, options) {
    this._callbackStack = new CallbackStack();
    this.selector = selector;
    this.$form = $(selector);
    this.debugToggle = false;
    this.options = $.extend({
      dataType: 'json',
      masks: {
        phone: '\+7(999) 999-99-99',
      },
      beforeSubmit: this.beforeSubmit,
      /**
       * Callback функция обработки ответа от сервера, после полученый ответа обработки формы.
       * @param data
       * @param statusText
       * @param xhr
       * @param $form
       * @returns {*}
       */
      success: this.success.bind(this),
      finder: {
        prefix: '',
        postfix: '',
      },
      callback: {
        /**
         * Если в ответе сервера имеется информация об ошибках заполнения формы
         * @param {Object} errors
         */
        isErrors: function(errors) {

        },
        /**
         * Как обрабатывать ответ с ключем result от сервера
         * @param {Object} results
         */
        isResults: function(results) {

        },
      },
    }, options);
    this.xhr = false;
    this.stack = null;
  }

  /**
   * Инициализация форм через метод ajaxSubmit
   * @returns {Form}
   */
  pluginSetup($this) {
    let instance = $this.ajaxSubmit(this.options).
        addClass('ajax-form-submited');
    this.xhr = instance.data('jqxhr');
    return instance;
  }

  /**
   * Инициализация форм через метод ajaxSubmit
   * @returns {Form}
   */
  initForm() {
    this._callbackStack.create('on submitted form number' + this.uniqueId);
    this.$form.on('submit', function(e) {
      e.preventDefault();
      e.stopPropagation();
      this.pluginSetup(this.$form);
    }.bind(this));
    return this;
  }

  /**
   * Действие до отправки формы на сервер
   * @param formData
   * @param $form
   * @param options
   */
  beforeSubmit(formData, $form, options) {
    //Работа с кнопкой submit
    $form.find('[type="submit"]').
        addClass('btn_progress').
        prop('disabled', true);
    $form.addClass('loading');
    if (this.hasOwnProperty('xhr') && this.xhr) this.xhr.abort();
  }

  /**
   * Стандартная обработка формы
   * @param data
   * @param statusText
   * @param xhr
   * @param $form
   */
  success(data, statusText, xhr, $form) {
    // Работа с полями которые имеют ошибки
    if (data.hasOwnProperty('errors') && typeof data.errors === 'object') {
      this.errorsHandler(data.errors, $form);
      this.options.callback.isErrors(data.errors, $form);
    }
    if (data.hasOwnProperty('result') && data.result.hasOwnProperty('success') && data.result.success) {
      Form.resultsHandler(data.result, $form);
      this._callbackStack.perform('on submitted form number' + this.uniqueId);
      this.options.callback.isResults(data.result, $form);
    }
    // Работа с полями которые прошли пероверку
    $form.find('.form-control:not(.is-invalid)').
        addClass('is-valid').
        on('change', function() {
          $(this).removeClass('is-valid');
        });
    $form.find('[type="submit"]').
        removeClass('btn_progress').
        prop('disabled', false).
        trigger('refresh.owl.carousel');
    $form.removeClass('loading');
  }

  onSubmitted(callback = null) {
    this._callbackStack.add('on submitted form number' + this.uniqueId, callback);
    return this;
  }

  /**
   * Обработчик ошибок формы
   * @param errors
   * @param $form
   */
  errorsHandler(errors, $form) {
    Object.keys(errors).forEach(function(key) {
      let value = errors[key];
      let name = this.options.hasOwnProperty('finder') ?
          this.options.finder.prefix + key + this.options.finder.postfix :
          key;
      let $input = $form.find('[name="' + name + '"]');
      let $parent = $input.parent();
      let $field = $input.closest('.field');
      let errorText = '';
      let $invalidFeedback = $parent.siblings('.invalid-feedback');

      if (!$invalidFeedback.length) {
        $invalidFeedback = $('<div class="invalid-feedback"></div>');
        $parent.after($invalidFeedback);
      }
      if (Array.isArray(value)) {
        value.forEach(function(item, i, value) {
          errorText = errorText + ('<li>' + item + '</li>');
        });
        errorText = '<ul>' + errorText + '</ul>';
      } else {
        errorText = value;
      }
      $invalidFeedback.html(errorText).show();
      $field.addClass('error');

      $input.trigger('invalid').
          removeClass('message valid').
          addClass('message invalid').
          on('focus', function() {
            $invalidFeedback.css({opacity: '0.5'});
          }).
          on('blur', function() {
            $invalidFeedback.css({opacity: '1'});
          }).
          on('change keyup', function() {
            $(this).removeClass('is-invalid');
            $field.removeClass('error');
            $invalidFeedback.hide();
          });

    }.bind(this));
  }

  static resultsHandler(results, $form) {
    if (results.hasOwnProperty('redirect') && results.redirect) {
      window.location.href = results.redirect;
    }
    if (results.hasOwnProperty('event') && results.event) {
      $form.trigger(results.event,
          results.hasOwnProperty('eventData') ? [results.eventData] : []);
    }
    if (results.hasOwnProperty('alert') && results.alert) {
      alert(results.message);
    }
    if (results.hasOwnProperty('reset') && results.reset === true) {
      $form.resetForm();
    }
    if (results.hasOwnProperty('message') && typeof results.message === 'string') {
      $form.append('<p class="message">' + results.message + '</p>');
    }
  }

  /**
   * Сериализация данных формы
   * @returns {*}
   */
  serialize() {
    return this.$form.formSerialize();
  }

  /**
   * Сброс данных формы
   * @returns {Form}
   */
  reset() {
    this.$form.resetForm();
    return this;
  }

  /**
   * Отобразаить поле как не верно заполненное
   * @param name
   * @returns {Form}
   */
  invalide(name) {
    this.$form.find('[name="' + name + '"]').addClass('was-invalid');
    return this;
  }

  /**
   * Получить jquery объект форм данного экземпляра.
   * @returns {*}
   */
  getForm() {
    return this.$form;
  }

  /**
   * Маска телефонного номера для input
   * @requires inputmask
   * @param selector
   * @param $context
   * @returns {Form}
   */
  setPhoneField(selector, $context = null) {
    let $field = $(selector, $context ? $context : this.$form);
    if ($field.length) {
      $field.each(function() {
        Inputmask({mask: '\+7(999) 999-99-99'}).mask(this);
      });
    }
    return this;
  }
}

export default Form;