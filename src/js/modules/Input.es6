import $ from 'jquery';
import ActionManager from './ActionManager';

class Input {
  constructor(selector, type) {
    this.rem = Number(getComputedStyle(document.body, null).
        fontSize.
        replace(/[^\d]/g, '')) || 16;
    this.selector = selector;
    this.$elements = $(selector);
  }

  /**
   * Инициализация числовых инпутов
   * @param selector
   * @returns {Input}
   */
  initNumbersInput(selector) {
    let $inputs = $(selector);
    if ($inputs) {
      $inputs.addClass('form-control-number');
      $inputs.each(function(index) {
        let template = {
          plus: $(
              `<div class="input-manipulator input-group-append form-group-number-control form-group-number-control_plus"><span class="input-group-text"></span></div>`),
          minus: $(
              `<div class="input-manipulator input-group-prepend form-group-number-control form-group-number-control_minus"><span class="input-group-text"></span></div>`),
        };
        let $this = $(this);
        if (!$this.val()) {
          $this.val(!!$this.attr('min') ? $this.attr('min') : 0);
        }

        $this.siblings('.input-manipulator').remove();
        template.minus.on('click', function(e) {
          e.preventDefault();
          (!$this.attr('min') || parseInt($this.val()) > $this.attr('min')) ?
              $this.val(parseInt($this.val()) - 1) :
              null;
        });
        template.plus.on('click', function(e) {
          e.preventDefault();
          (!$this.attr('max') || parseInt($this.val()) < $this.attr('max')) ?
              $this.val(parseInt($this.val()) + 1) : null;
        });
        $this.after(template.plus).
            after(template.minus).
            on('change', function(e) {
              if (!$this.val()) {
                $this.val($this.attr('min'));
              }
              if ($this.attr('max') && $this.val() > $this.attr('max')) {
                $this.val($this.attr('max'));
              }
            });
      });
    }
    return this;
  }

  /**
   * Инициализация контрола "стереть все"
   * @returns {Input}
   * @param controll
   */
  eraseControl(controll = '.remove.icon.link') {
    $(document).on('click', controll, function(e) {
      e.preventDefault();
      $(this).siblings('input').val(null);
    });
    return this;
  }

  /**
   * Инициализация контрола password\text
   * @returns {Input}
   */
  passwordControl(context) {
    let $inputs = context ? $('input[type="password"]', context) : $('input[type="password"]');
    $inputs.each(function(index) {
      let $input = $(this);
      let $parent = $input.parent();
      let $control = $input.siblings('.advanced.icon.eye');
      $control.length === 0 && ($control = $('<i class="advanced icon link eye">'));
      $control.
          off('click').
          on('click', function() {
            let $control = $(this);
            if ($input.is('[type="password"]')) {
              $input.attr('type', 'text');
              $control.addClass('open');
            } else {
              $input.attr('type', 'password');
              $control.removeClass('open');
            }
          });
      if ($parent.is('.input')) {
        $input.after($control);
      }
    });
    return this;
  }

  /**
   * Инициализация контрола кнопки поиска
   * @param selector
   * @returns {Input}
   */
  searchControl(selector) {
    let $inputs = $(selector);
    if ($inputs.length) {
      $inputs.each(function(index) {
        let $input = $(this);
        let $template = $(
            '<button type="submit" data-input-control="search" class="form-search-control"></button>');
        let container = '.form-controls-container';
        let $containerTemp = $('<div class="form-controls-container"></div>');
        let $container = $input.siblings(container);
        if (!$container.length) {
          $container = $containerTemp;
          $input.after($container);
        }
        if (!$container.find('.form-search-control').length) {
          $container.append($template);
        }
      });
    }
    return this;
  }

  /**
   * Маска телефонного номера для input
   * @requires inputmask
   * @param selector
   * @returns {Input}
   */
  phoneControl(selector) {
    let $inputs = $(selector);
    if ($inputs.length) {
      $inputs.each(function(index) {
        $(this).inputmask({mask: '\+7(999) 999-99-99'});
      });
    }
    return this;
  }

  /**
   * Маска и проверка поля email
   * @requires inputmask
   * @param selector
   * @returns {Input}
   */
  emailControl(selector) {
    let $inputs = $(selector);
    if ($inputs.length) {
      $inputs.each(function(index) {
        $(this).inputmask({
          greedy: false,
          regex: '[a-za-zA-Z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?',
        });
      });
    }

    return this;
  }

  /**
   * Фикстуры для инпутов - для корректного отображения.
   * @returns {Input}
   */
  static fixtures() {
    let fixControlContainer = function(remSizeLocal = null) {
      let remSize;
      if (!remSizeLocal) {
        remSize = remSizeLocal;
      } else {
        remSize = Number(getComputedStyle(document.body, null).fontSize.replace(/[^\d]/g, '')) || 16;
      }
      let $containers = $('.form-controls-container');
      $containers.each(function(index) {
        let $container = $(this);
        let $input = $container.siblings('.form-control').eq(0);
        let containerWidth = function() {
          let width = 0;
          $container.children().each(function() {
            width += $(this).innerWidth();
          });
          width += $input.is('.form-control-erase') ? remSize : 0;
          return width;
        }();
        let inputWidth = $input.innerWidth();
        console.log(inputWidth, containerWidth, Input.rem(inputWidth - (remSize * 1), true));
        $container.css({
          right: 'auto',
          left: Input.rem(inputWidth - (remSize * 1), true),
          opacity: 1,
        });
        $input.css('padding-right', Input.rem(containerWidth + remSize * 2 +
            (remSize * $container.children().length / 2), true));
      });
    };
    $(window).on('resize', fixControlContainer);
    fixControlContainer();
    $('.form-info-control').
        siblings('.form-controls-container').
        addClass('input-fixtures-info-control');
    return this;
  }

  static rem(value, postfix) {
    return value / Number(getComputedStyle(document.body, null).fontSize.replace(/[^\d]/g, '')) || 16 +
        (postfix ? 'rem' : '');
  }

  static load(){
    !window.hasOwnProperty('$$') && (window.$$ = {});
    !window.$$.hasOwnProperty('Input') && (window.$$.Input = new Input());
    return window.$$.Input;
  }

}

export default Input;