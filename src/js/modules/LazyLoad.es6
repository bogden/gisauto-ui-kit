import DomPluginBone from '../bones/DomPluginBone';
import LazyLoadPlugin from 'lazyload';

class LazyLoad extends DomPluginBone{
  /**
   * Инициализатор "Ленивой" загрузки
   * расширяется с помощью DomPluginBone
   * и использует нативную выборку по DOM
   */
  constructor() {
    super();
    this.selector = null;
    this.lazyInstance = [];
    this.setManipulator('native');
  }

  /**
   * Инициализация ленивой загрузки и привязка событий
   * Для переинициализации "Ленивой загрузки" на странице достаточно вызвать
   * событие 'init.lazy' на document
   * @return {LazyLoad}
   */
  lazyload(){
    this.lazyLoad = new LazyLoadPlugin(this.getElements());
    return this;
  }

  /**
   * Добавить событие по которому
   * пройдет переинициализация плагина
   * @param eventName
   * @return {LazyLoad}
   */
  addEvent(eventName = 'lazyload'){
    document.addEventListener(eventName, this.lazyload);
    return this;
  }
}

export default LazyLoad;