import $ from 'jquery';
import '@fancyapps/fancybox';
import 'Semantic/search';
import Preloader from 'Modules/Preloader';
import Router from 'Modules/Router';
import Modal from './Modal';

class LocationSelect {
  constructor(id, options = {}) {
    this.id = id;
    this.query = '';
    this.options = $.extend({
      cityListData: '/city-map-list',
      locationData: '/get-cities-data.json',
    }, options);
    this.data = {
      cities: [],
      nearCities: [],
    };
    this.preloader = new Preloader('body', {
      theme: 'light',
    });
  }

  selectLocation(){
    this.getCityMapList(this.options.cityListData);
    this.getCitiesData(this.options.locationData);
    Modal.openAjaxStatic('selectLocation', Router.generate('get_modal_city', {}, window.location.host), {type: 'GET'}, {
      header: true,
      title: 'Указать город',
    }, {
      afterShow :  function(instance){
        let $search = instance.current.$content.find('.ui.search');
        this.notFoundMessage($search);
        $(document).on('click change', '[data-set-location]', function(e) {
          e.preventDefault();
          e.preventDefault();
          this.preloader = new Preloader('#location-modal', {theme: 'light'});
          this.preloader.show();
          this.setCurrentLocation($(e.target).data('set-location'), false);
        }.bind(this));
        $search.search({
          source: this.data.cities,
          maxResults: 8,
          fields: {
            title: 'name',
            url: 'html_url',
            description: 'region_name',
          },
          searchFields: ['name', 'region_name'],
          onSearchQuery: function(q) {
            this.query = q;
          }.bind(this),
        });
        $('.ui.accordion').accordion();
      }.bind(this)
    });
  }

  /**
   * Действие после открытия модального окна
   * @param instance
   */
  afterShow(instance) {

  }

  nearSearch(query = null) {
    if (!query) {
      query = this.query;
    }
    if (!query) {
      return null;
    }
    let regExp = null;
    let result = null;
    $.each(this.data.nearCities, function(key, value) {
      regExp = new RegExp(query, 'i');
      if (value.name.search(regExp) >= 0) {
        result = value;
      }
    });
    return result;
  }

  notFoundMessage($search) {
    $.fn.search.settings.templates.message = function(message, type) {
      let html = '';
      if (message !== undefined && type !== undefined) {
        if (type === 'empty') {
          let near = this.nearSearch($search.search('get value'));
          if (near) {
            html += '<div class="message ' + type +
                '"><div class="header">В городе ' + near.name +
                ' не найдено активных продавцов.</div class="header"><div class="description">Выберите один из ближайших городов.</div class="description"></div>';
            $.each(near['near_cities'], function(key, value) {
              html += '<a class="result"><div class="content"><div class="title">' +
                  value.name + '</div></div></a>';
            });
          } else {
            html += '<div class="message ' + type +
                '"><div class="header">Городов по вашему запросу не найдено</div class="header"><div class="description">Укажите другой город или проверьте правильность написания.</div class="description"></div>';
          }
        } else {
          html += '<div class="message ' + type +
              '"><div class="description">' + message + '</div></div>';
        }
      }
      return html;

    }.bind(this);
  }

  getCityMapList(url = '/city-map-list') {
    $.ajax({
      url: url,
      dataType: 'json',
      localCache: true,
      cacheTTL: 96,
      cacheKey: 'cityMapList',
      success: function(data) {
        this.data.cities = data.data.cities;
      }.bind(this),
      error: function(e) {
        console.error(e);
      },
    });
  }

  getCitiesData(url = '/get-cities-data.json') {

    if (window.hasOwnProperty('sessionStorage') && sessionStorage.getItem('nearCities')) {
      this.data.nearCities = JSON.parse(sessionStorage.getItem('nearCities'));
    } else {
      $.ajax({
        url: url,
        dataType: 'json',
        localCache: true,
        cacheTTL: 96,
        cacheKey: 'nearCities',
        success: function(data) {
          this.data.nearCities = data;
        }.bind(this),
        error: function(e) {
          console.error(e);
        },
      });
    }
  }

  setCurrentLocation(locationId, autoDetectLocation) {
    locationId = locationId ? locationId : 0;
    autoDetectLocation = autoDetectLocation ? autoDetectLocation : false;
    $.ajax({
      url: Router.generate('set_web_current_location_ajax', {location: locationId}, window.location.host),
      type: 'POST',
      localCache: false,
      dataType: 'json',
      data: {
        'locationId': locationId,
        'autoDetectLocation': autoDetectLocation,
      },
      timeout: 10000,
      success: function(response) {
        location.reload();
      },
    });
  }

  static load(){
    !window.hasOwnProperty('$$') && (window.$$ = {});
    !window.$$.hasOwnProperty('LocationSelect') && (window.$$.LocationSelect = new LocationSelect());
    return window.$$.LocationSelect;
  }
}

export default LocationSelect;