import $ from 'jquery';
import '@fancyapps/fancybox';
import ActionManager from './ActionManager';

class Modal {
  constructor() {
    this.modalPlugin = null;
    this.modals = {};
    this.ajaxStack = [];
    this.emitter = function() {
      $(document).trigger('modal.open');
    };
  }

  /**
   * Инициализация плагина Fancybox
   */
  initFancybox(options = {}) {
    if (typeof $.fn.fancybox !== 'undefined') {
      options = $.extend({
        selector: '[data-fancybox]',
        buttons: ['close'],
        animationEffect: 'zoom',
        animationDuration: 200,
        zoomOpacity: 'auto',
        transitionEffect: 'zoom-in-out',
        lang: 'ru',
        i18n: {
          'ru': {
            CLOSE: 'Закрыть',
            NEXT: 'Далее',
            PREV: 'Назад',
            ERROR: 'The requested content cannot be loaded. <br/> Please try again later.',
            PLAY_START: 'Запустить слайдшоу',
            PLAY_STOP: 'Пауза',
            FULL_SCREEN: 'На весь экран',
            THUMBS: 'Миниатюры',
            DOWNLOAD: 'Загрузить',
            SHARE: 'Поделится',
            ZOOM: 'Увеличить',
          },
        },
      }, options);
      $().fancybox(options);
      this.modalPlugin = 'fancybox';
    } else {
      console.error('Fancybox not defined');
    }
    return this;
  }

  /**
   * Инициализируем общий action manager
   * в window если он присутствует.
   */
  initActionManager() {
    if (window.$$.hasOwnProperty('ActionManager')) {
      this.emitter = function(command) {
        window.$$.ActionManager.perform(command);
      };
    } else {
      console.warn('Action manager is not defined');
    }
    return this;
  }

  open(name, content, params, pluginParams) {
    Modal.openStatic(name, content, params, pluginParams);
    return this;
  }

  openAjax(name, url, ajaxParams = {}, params = {}, pluginParams = {}){
    if(typeof name === 'string' && typeof url === 'string'){
      if(this.ajaxStack[name] && this.ajaxStack[name].hasOwnProperty('abort')){
        this.ajaxStack[name].abort();
      }
      this.ajaxStack[name] = Modal.openAjaxStatic(name, url, ajaxParams, params , pluginParams);
    }else{
      console.warn('Name or URL not string')
    }
    return this;
  }

  static openAjaxStatic(name, url, ajaxParams, params = {}, pluginParams = {}) {
    ajaxParams = $.extend({
      url: url,
      localCache: true,
      cacheTTL: 0.004166667,
    }, ajaxParams, {
      success: function(data) {
        switch (typeof data) {
          case 'string':
            Modal.openStatic(name, data, params, pluginParams);
            break;
          case 'object':
            Modal.openStatic(name, data.data.html, params, pluginParams);
            break;
          default:
            break;
        }
      },
      error: function() {
        console.warn('Data not found');
      },
    });
    return $.ajax(ajaxParams);
  }

  static openStatic(name, content = '', params = {}, pluginParams = {}) {
    pluginParams = $.extend(true,  {
      modal: true,
      buttons:  false,
      smallBtn: false
    },pluginParams);
    console.log(pluginParams);
    $.fancybox.open(Modal.generateTemplate(name, content, params), pluginParams);
  }

  static generateTemplate(id, content, params) {
    params = $.extend(true, {
      classes: '',
      footer: false,
      header: false,
      title: '',
      footerContent: ''
    }, params);
    let template = `
        <div class="kit modal` + (params.header ? ' with darken header ' : ' ') + params.classes + `" id="` + id + `">
            <div class="container">` + (params.header === true ?
        `<div class="header">` + (params.title.length > 1 ? '<div class="title">' + params.title + '</div>' : '') + `<button class="close" data-fancybox-close>✕</button> </div>` :
        '') +
        `<div class="body">` + content + `</div>` +
        (params.footer === true ? (`<div class="footer">` + params.footerContent + `</div>`) : '') +
        `</div>
        </div>
      `;
    return template;
  }

  static load() {
    !window.hasOwnProperty('$$') && (window.$$ = {});
    !window.$$.hasOwnProperty('Modal') && (window.$$.Modal = new Modal());
    window.$$.Modal.initFancybox();
    return window.$$.Modal;
  }
}

export default Modal;