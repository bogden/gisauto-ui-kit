import $ from 'jquery';

class NavStack {
  /**
   * Навигация на странице в виде "экранов приложения"
   * @param stack
   * @param options
   */
  constructor(stack, options = {}) {
    this.$el = $(stack);
    this.options = $.extend({
      animation: 'fadeIn',
      hash: true,
    }, options);
    this.active = null;
    this.screens = [];
    this.history = ['default'];
    this.$window = $(window);
    this.$footer = $('#siteFooter');
    this.$header = $('#siteHeader');

    this.rebuildDOM();
    this.listeners();
    this.loaded();
    this.fix();

    this.options.hash
    && (this.hash = this.getHash())
    && this.hashHandler();
  }

  loaded() {
    this.$el.removeClass('loading').addClass('loaded');
    this.emitter('loaded', this.$el);
  }

  rebuildDOM() {
    this.$el.addClass('kit screens ' + this.options.animation).
        children().
        each(function(key, item) {
          let $this = $(item);
          let name = $this.data('screen');
          if ($this.is('[data-screen]') && name) {
            $this.is('[data-active]') && (this.active = name);
            this.screens[name] = $this.wrap(
                '<div class="screen screen-' + name + '" data-name="' + name + '"></div>').parent();
          }
        }.bind(this));
    if (!this.active) {
      let $first = this.$el.children('.screen:first');
      this.active = $first.data('name');
    }
    this.push(this.active);

  }

  hashHandler() {
    $(document).ready(function() {
      this.push(this.getHash(), {errors: false});
    }.bind(this));
  }

  /**
   * Добавить экран с указаным названием
   * @param name
   * @param content
   */
  add(name, content) {
    if (this.screens.hasOwnProperty(name)) {
      console.warn('The panel already existed on the stack and was overwritten.');
    }
    this.screens[name] = $(content).
        wrap('<div class="screen screen-' + name + '"></div>');
    this.$el.append(this.screens[name]);
    return this;
  }

  /**
   * Обработка изменения hash
   */
  listeners() {
    $(window).on('hashchange', function() {
      this.hash = window.location.hash || '#default';
      if (this.get(name)) {
        this.push(this.getHash());
      }
    }.bind(this));
  }

  /**
   * Установить hash
   * @param hash
   * @return {NavStack}
   */
  setHash(hash = '') {
    this.hash = window.location.hash.replace(/^#/, '') || 'default';
    $(window).trigger('hashchange');
    window.location.hash = '#' + hash;
    return this;
  }

  /**
   * Получить hash
   * @return {*|string}
   */
  getHash() {
    return this.hash || window.location.hash.replace(/^#/, '');
  }

  /**
   * Получить имя активного экрана
   * @return {*}
   */
  getActive() {
    return this.active;
  }

  /**
   * Получить экран
   * @param name
   * @return {null}
   */
  get(name) {
    return this.screens.hasOwnProperty(name) ? this.screens[name] : null;
  }

  /**
   * Вывести указанный экран
   * @param name
   * @param options
   * @return {NavStack}
   */
  push(name = '', options = {}) {
    options = $.extend({
      errors: true,
      hash: this.options.hash,
      history: true,
    }, options);
    if (this.get(name)) {
      options.hash && this.setHash(name);
      let $children = this.$el.children();
      let $previous = $children.filter('.screen.active').
          removeClass('active').
          addClass('previous');
      setTimeout(function() {
        $previous.removeClass('previous');
      }, 1500);
      this.screens[name].addClass('active');
      options.history && this.history.push(name);
      this.active = name;
      this.emitter('push', name);
      this.fix();
    } else {
      if (options.errors) {
        console.warn('The specified screen does not exist');
      }
    }
    return this;
  }

  emitter(event = '', data) {
    (this.options.hasOwnProperty('vue') &&
        typeof this.options.vue.$emit === 'function' &&
        this.options.vue.$emit(event, data)) || this.$el.trigger(event, data);
    return this;
  }

  fix() {
    let $window = this.$window;
    let $footer = this.$footer;
    let $header = this.$header;
    $('.screens').each(function() {
      let $this = $(this);
      $this.css({
        'min-height': ($window.height() - $this.offset().top -
            ($footer.height() || 0) - $header.height() || 0) + 'px',
      });
    });
    return this;
  }

  back(options) {
    options = $.extend({history: false}, options);
    this.backAvailable() && (this.history.pop() && this.push(this.history[this.history.length - 1], options));
    return this;
  }

  backAvailable() {
    return this.history.length > 1;
  }

  setWay(way) {
    typeof way === 'array' && (this.way = way);
    return this;
  }

}

export default NavStack;