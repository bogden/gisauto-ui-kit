import $ from 'jquery';
import {watch} from 'watchjs/src/watch';

class Preloader {
  /**
   * Создание экземпляра прелоада
   * @param {String} selector
   * @param {Object} options
   */
  constructor(selector = 'body', options = {}) {

    this.options = $.extend(Preloader.getOptions(), options);
    this.$container = $(selector) || $(document);
    this.$preloader = $('<div class="kit preloader ' + this.options.theme + '"></div>');
    this.state = {
      active: false,
    };
    this.init().listeners();
    watch(this.state, 'active', this.handleState);

  }

  /**
   * Инициализация прелоада в контейнер
   * @return {Preloader}
   */
  init() {
    let $search = this.$container.find('.kit.preloader');
    if (!$search.length) {
      this.$container.append(this.$preloader);
    } else {
      this.$preloader = $search;
    }
    if (this.state.active) {
      this.show();
    }
    return this;
  }

  /**
   * Переключение состояний прелоада
   */
  handleState() {
    this.state.active ? this.show() : this.hide();
  }

  /**
   * Показать прелоадер
   * @return {Preloader}
   */
  show() {
    if (this.$preloader) {
      this.$preloader.addClass('active');
      if (this.options.timeout) {
        setTimeout(this.hide, this.options.timeout);
      }
    }
    return this;
  }

  /**
   * Скрыть прелоадер
   * @return {Preloader}
   */
  hide() {
    if (this.$preloader) {
      this.$preloader.removeClass('active');
    }
    return this;
  }

  /**
   * получение апций для инициализации
   * @return {{theme: string, timeout: number, onActivated: *, onAfterHide: *}}
   */
  static getOptions() {
    return {
      theme: 'light',
      timeout: 1000,
      onActivated: void 0,
      onAfterHide: void 0,
    };
  }

  /**
   * Обработчики событий
   * @return {Preloader}
   */
  listeners() {
    this.$container.on('preloader.show', this.show).on('preloader.hide', this.hide);
    return this;
  }

  /**
   * Уничтожить прелоад
   * @return {null}
   */
  destroy() {
    this.$container.off('preloader');
    this.$preloader.remove();
    return null;
  }

  /**
   * Добавить контролл
   * @param $element
   * @param eventName
   * @param action
   * @return {Preloader}
   */
  control($element, eventName, action = 'show') {
    switch (action) {
      case 'show':
        $element.on(eventName, function() {
          this.show();
        }.bind(this));
        break;
      case 'hide':
        $element.on(eventName, function() {
          this.hide();
        }.bind(this));
        break;
      default:
        console.error('Неверно инициализирован контролл прелоада');
        break;
    }
    return this;
  }
}

export default Preloader;
