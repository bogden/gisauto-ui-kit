import $ from 'jquery';
import DomPluginBone from '../bones/DomPluginBone';
import 'Semantic/rating';
import Alert from './Alert';

class Rating extends DomPluginBone{
  /**
   * Рейтинги элементов
   * данные класс реализует интерфес
   * для отображения и установки рейтингов на сайте.
   */
  constructor() {
    super();
    this.setManipulator('jquery');
  }

  /**
   * Инициализация плагина
   */
  initRatings(params = {}) {
    this.element.each(function(index, element) {
      let $this = $(element);
      let interactive = $this.data('interactive');
      let params = $.extend({}, this.params, {
          interactive: interactive ? (interactive === 'on') : this.params.interactive
        });
      $this.rating(params);
    }.bind(this));
    return this;
  }

  /**
   *  Инициализации плагина для одного элемента
   */
  initRating(params = {}){
    this.element.rating($.extend(Rating.defaultParams(), params));
  }

  /**
   * Стандартные настроки для плагина
   * @returns {{initialRating: number, fireOnInit: boolean, clearable: string, interactive: boolean, onRate: Rating.onRate}}
   */
  static defaultParams() {
    return {
      initialRating: 0,
      fireOnInit: false,
      clearable: 'auto',
      interactive: false,
      onRate: this.onRate
    };
  }

  /**
   * Обработчик изменения состояние рейтинга
   * @param value
   */
  handleRating(value) {

  }

  /**
   * Обработчик события установки рейтинга
   * @param value
   */
  onRate(value) {
    let $this = $(this);
    $this.rating('disable');
    this.handleRating(value);
  }

  /**
   * Выключить интерактивную установку рейтинга
   * @param filter
   * @returns {Rating}
   */
  disable(filter) {
    this.$elements.filter(filter).rating('disable');
    return this;
  }

  /**
   * Включить интерактивую установку рейтинга
   * @param filter
   * @returns {Rating}
   */
  enable(filter) {
    this.$elements.filter(filter).rating('enable');
    return this;
  }

  /**
   * Сбросить рейтинг
   * @param filter
   * @returns {Rating}
   */
  clearRating(filter) {
    this.$elements.filter(filter).rating('clear rating');
    return this;
  }

  /**
   * Получить рейтинг
   * @param filter
   * @returns {Rating}
   */
  getRating(filter) {
    this.$elements.filter(filter).rating('get rating');
    return this;
  }

  /**
   * Установить рейтинг
   * @param filter
   * @param rating
   * @returns {Rating}
   */
  setRating(filter, rating) {
    rating = parseFloat(rating);
    this.$elements.filter(filter).rating('set rating', rating);
    return this;
  }

  static rating(element, params = {}){
    $(element).rating(params);
    return $(element);
  }

  static load(){
    !window.hasOwnProperty('$$') && (window.$$ = {});
    !window.$$.hasOwnProperty('Rating') && (window.$$.Rating = new Rating());
    return window.$$.Rating;
  }
}

export default Rating;