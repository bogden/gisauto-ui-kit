/**
 * Class Router
 */
class Router {

  /**
   * @constructor
   * @param {Router.Context=} context
   * @param {Object.<string, Router.Route>=} routes
   */
  constructor(context, routes) {
    this.host = null;
  }

  /**
   * Generates the URL for a route.
   *
   * @param {string} name
   * @param {Object.<string, string>} opt_params
   * @param {string} host
   * @param {boolean} absolute
   * @return {string}
   */
  static generate(name, opt_params, host = null, absolute) {
    return host
        ? Routing.generate(name, opt_params, absolute).replace(/^((?:https?:\/\/))?([a-zA-Z0-9\.\-_]+)/, '$1' + host)
        : Routing.generate(name, opt_params, absolute);
  }

  setHost(host) {
    this.host = host;
  }

  getHost() {
    return this.host;
  }
}

export default Router;