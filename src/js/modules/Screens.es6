import $ from 'jquery';
class Screens {
  constructor(container, options = {}) {
    this.container = container;
    this.$container = $(container);
    this.options = $.extend({
      start: '#index',
      toggle: '[data-target]',
      back: '[data-back]',
      containers: ''
    }, options);
    this.$screens = this.$container.find('.screen');
    this.history = [this.options.start];
  }

  /**
   * Инициализация плагина
   * @return {Screens}
   */
  init() {
    this.$container.on('click', this.options.toggle, function(e) {
      e.preventDefault();
      let $this = $(e.target);
      let target = $this.data('target');
      this.show(target);
    }.bind(this)).on('click', this.options.back, function(e) {
      e.preventDefault();
      this.back();
    }.bind(this)).addClass('start');
    return this;
  }

  /**
   * Показать экран
   * @param target
   * @param addToHistory
   * @return {Screens}
   */
  show(target, addToHistory = true) {
    let $target = this.$screens.filter(target);
    if ($target.is('.screen')) {
      this.$screens.removeClass('active');
      $target.addClass('active');
      if (addToHistory) {
        this.addHistory(target);
      }
    }
    if (this.history[this.history.length - 1] !== this.options.start) {
      this.$container.removeClass('start');
    }else{
      this.$container.addClass('start');
    }
    return this;
  }

  /**
   * Добавление экрана в историю
   * @param target
   * @return {Screens}
   */
  addHistory(target) {
    this.history.push(target);
    return this;
  }

  /**
   * Вернуться на прошлый экран
   * @return {Screens}
   */
  back() {
    console.log(this.history);
    if (this.history.length > 1) {
      this.history.pop();
      this.show(this.history[this.history.length - 1], false);
    } else {
      window.history.back();
    }
    return this;
  }
}

export default Screens;
