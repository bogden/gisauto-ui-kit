import $ from 'jquery';
import 'Semantic/search';

class Search {
  constructor() {
    this.$context = undefined;
    this.options = {};
    this.presets = {
      'part helper': {
        apiSettings: {
          url: decodeURI(Routing.generate('part_helper', {searchString: '{query}'})),
        },
        cache: true,
        selectFirstResult: true,
        showNoResults: false,
        fields: {
          results: 'data',
          title: 'name',
        },
        error : {
          source      : 'Cannot search. No source used, and Semantic API module was not included',
          noResults   : 'Your search returned no results',
          logging     : 'Error in debug logging, exiting.',
          noTemplate  : 'A valid template name was not specified.',
          serverError : 'There was an issue with querying the server.',
          maxResults  : 'Results must be an array to use maxResults setting',
          method      : 'The method you called is not defined.'
        },
        minCharacters: 1,
      }
    };
  }

  init(selector, options = {}, preset = null) {
    this.$elements = $(selector, this.$context);
    this.setOptions(options, preset);
    this.$elements.search(this.options);
    return this;
  }

  wrap($element) {
    if (!$element.is('.prompt')) {
      $element.addClass('prompt').wrap('<div class="ui search"></div>');
      $element.after('<div class="results"></div>');
    }
  }

  setContext($context = null) {
    this.$context = $context ? $context : $(document);
    return this;
  }

  setOptions(options, preset = null) {
    this.options = $.extend(this.options, (preset ? this.getSearchPreset(preset) : {}), options);
    return this;
  }

  getSearchPreset(name) {
    if (name && this.presets.hasOwnProperty(name)) {
      return this.presets[name];
    }
    return {};
  }
}

export default Search;