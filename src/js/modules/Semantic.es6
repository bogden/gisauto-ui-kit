import $ from 'jquery';

class Semantic {
  constructor() {
    /**
     * Настройки для Search
     * @type {{source: string, noResults: string, logging: string, noEndpoint: string, noTemplate: string, serverError: string, maxResults: string, method: string}}
     */
    if ($.fn.hasOwnProperty('search')) {
      $.fn.search.settings.error = {
        source: 'Невозможно выполнить поиск. Источник не использовался, и модуль Semantic API не был включен',
        noResults: 'Ваш запрос не дал результатов',
        logging: 'Error in debug logging, exiting.',
        noEndpoint: 'Конечная точка поиска не указана.',
        noTemplate: 'A valid template name was not specified.',
        serverError: 'There was an issue querying the server.',
        maxResults: 'Results must be an array to use maxResults setting',
        method: 'The method you called is not defined.'
      };
      $.fn.search.settings.templates.message = function(message, type) {
        let html = '';
        if (message !== undefined && type !== undefined) {
          html += '' + '<div class="message ' + type + '">';
          if (type === 'empty') {
            html += '' + '<div class="header">Нет результатов</div class="header">' + '<div class="description">' + message + '</div class="description">';
          } else {
            html += '<div class="description">' + message + '</div>';
          }
          html += '</div>';
        }
        return html;
      };
    }
  }
}


export default Semantic;