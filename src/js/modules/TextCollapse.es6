import $ from 'jquery';

class TextCollapse {
  /**
   * Класс для работы с текстом
   * Предназначен для "сворачивания" длинного текста.
   * @param options
   */
  constructor(options) {
    this.options = $.extend({
      revealText: 'Подробнее',
      hideText: 'Короче',
      linesNumber: 4,
      hideMore: true,
      dots: true,
    }, options);
  }

  apply($elements) {
    let $more;
    $elements.each(function(index, item) {
      let $this = $(item);
      let height = $this.outerHeight();
      let lineHeight;
      $more = $(
        '<a class="text-collapse-more">' + (this.options.dots ? '...' : '') +
        '<span class="text-collapse-more-reveal">' + this.options.revealText +
        '</span></a>').on('click', function(e) {
        e.preventDefault();
        if ($this.is('.more')) {
          $this.height(lineHeight * this.options.linesNumber).
            removeClass('more').
            find('span.text-collapse-more-reveal').
            text(this.options.revealText);
        } else {
          $this.height(height).
            addClass('more').
            find('span.text-collapse-more-reveal').
            text(this.options.hideText);
        }
        if (this.options.hideMore) {
          $(e.target).remove();
        }
      }.bind(this));

      function getBackgroundColorComputed($element) {
        let computedStyle = getComputedStyle($element.get(0));
        return computedStyle.backgroundColor !== 'rgba(0, 0, 0, 0)' ?
          computedStyle.backgroundColor :
          getBackgroundColorComputed($element.parent());
      }

      $more.css({'background': getBackgroundColorComputed($this)});
      $this.append($more);
      lineHeight = $more.outerHeight() | 0;
      $this.height(lineHeight * this.options.linesNumber);
    }.bind(this));
    return this;
  }

  /**
   * Инициализация плагина jQuery
   */
  static initPlugin() {
    $.fn.textCollapse = function(options) {
      let textCollapse = new TextCollapse(options);
      textCollapse.apply(this);
      return this;
    };
  }
}

export default TextCollapse;