import $ from 'jquery';

class Textarea {
  constructor(selector) {
    this.selector = selector;
    this.$element = $(selector);
    this.$document = $(document);
  }

  /**
   * Автовысота для textarea
   */
  autoExpand() {
    if (this.$element.is('textarea')) {
      this.$document.on('keydown', this.selector, function(e) {
        setTimeout(function() {
          e.target.style.cssText = 'height:auto; padding:0';
          e.target.style.cssText = 'height:' + this.$element.get(0).scrollHeight + 'px';
        }.bind(this), 5);
      }.bind(this));
    }
  }
}

export default Textarea;