import $ from 'jquery';

class Validator{
  constructor(value = null, type = null, options = {}){
    this.options = $.extend({
      onReckoningReturn: false
    }, options);
    this.value = value;
    this.type = type;
  }

  check(value = null, type = null, options = {}){
    value = value ? value : this.value;
    type = type ? type : this.type;
    options = $.extend({
      layout: 'eng'
    }, options);
    switch(type) {
      case 'vin':
        return Validator.checkVin(value);
      case 'phone':
        return Validator.checkPhone(value);
      case 'email':
        return Validator.checkEmail(value);
      case 'notBlank':
        return Validator.notBlank(value);
      case 'string':
        return (options.layout === 'eng' && Validator.stringLayoutEng(value))
            || (options.layout === 'rus' && Validator.stringLayoutRus(value))
            || (options.layout === 'num' && Validator.stringLayoutNum(value));
      default:
        console.warn('Validator type \'' + type + '\' is not find');
        return this.options.onReckoningReturn;
    }
  }

  /**
   * Не пустое значение
   * @param value
   * @returns {boolean}
   */
  static notBlank(value){
    return typeof value !== 'undefined'
        && typeof value !== 'function'
        && value !== null
        && ((typeof value === 'string'
            && value.length) || (typeof value !== 'string')
        );
  }

  /**
   * Это строка?
   * @param value
   * @return {boolean}
   */
  static isString(value){
    return typeof value === 'string';
  }

  /**
   * Выполнить проверку
   * @param value
   * @returns {boolean}
   */
  static checkVin(value){
    return  Validator.notBlank(value) && !!value.match(/[0-9a-zA-Z\-]{8,20}(\n|$)/g);
  }

  /**
   * Проверить номер телефона
   * @todo Проверка еще не написана
   * @param value
   * @returns {boolean}
   */
  static checkPhone(value){
    return false;
  }

  /**
   * Проверка Email
   * @param value
   * @returns {boolean}
   */
  static checkEmail(value){
    return typeof Validator.notBlank(value) && Validator.isString(value) && !!value.match(/^(\w([.-]?))+\@\w+\.\w+$/);
  }

  static stringLayoutEng(value){
    return typeof value === 'string' && !value.match(/[а-яА-Я]{1,}/g)
  }

  static stringLayoutRus(value){
    return typeof value === 'string' && !value.match(/[a-zA-Z]{1,}/g)
  }

  static stringLayoutNum(value){
    return typeof value === 'number' || (typeof value === 'string' && !value.match(/[0-9]{0,}/g));
  }

}

export default Validator;