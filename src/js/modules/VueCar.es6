import $ from 'jquery';
import Car from '../api/Car';

let carMixin = {
  created() {
  },
  data() {
    return {
      car: new Car({}, {
        dispatcherType: 'vue',
        vueInstance: this,
        watch: true,
      }),
    };
  },
  methods: {
    /**
     * Проверка: Заполненна вся необходимая информация об автомобиле или нет
     * @return {*}
     */
    carAvailable() {
      return this.car && this.car.checkCar();
    },
  },
};

export default carMixin;