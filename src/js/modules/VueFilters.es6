let mixins = {
  filters: {
    capitalize(value) {
      if (!value) return '';
      value = value.toString();
      return value.charAt(0).toUpperCase() + value.slice(1)
    },
    comma(value){
      typeof value === 'string'
      && value.length > 0
      && (value += ', ');
      return value
    },
    default(value, defaultValue){
      if(!value) return defaultValue;
      return value;
    }
  }
};

export default mixins;