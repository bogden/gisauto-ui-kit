import $ from 'jquery';
import NavStack from 'Modules/NavStack';

/**
 * Миксин для Vue позволяющий работать с NavStack
 * @type {{mounted(): void, methods: {go(*=): *}}}
 */
let VueNavStack = {
  data(){
    return {
      screensSelector: null
    }
  },
  mounted() {
    this.screens = new NavStack(this.$data.screensSelector, {
      vue: this,
      hash: false,
    });
    this.$on('push', function(screen) {
      this.hasOwnProperty('onPush') && this.onPush(screen);
    }.bind(this));
  },
  methods: {
    go(screen) {
      this.screens.push(screen, {
        hash: false,
      });
      return this;
    },
    getActive(){
      return this.screens.getActive();
    }
  }
};

export default VueNavStack;