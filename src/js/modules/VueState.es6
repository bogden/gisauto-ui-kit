import $ from 'jquery';

let stateMixin = {
  data() {
    return {
      state: {
        screen: null
      }
    };
  },
  methods: {
    setState(state){
      this.$data.state = $.extend(this.$data.state, state);
    },
    defineState(state){
      this.$data.state = state;
    }
  }
};

export default stateMixin;