import Dropdown from 'Modules/Dropdown';

let DropdownDirective = {
  inserted(el, binding) {
    (new Dropdown()).init(el, {
      onChange: Dropdown.onChange
    });
  },
};
export default DropdownDirective;