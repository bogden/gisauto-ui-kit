import Rating from 'Modules/Rating';

let RatingDirective = {
  inserted(el, binding) {
    el.dataset.rating = binding.value;
    el.dataset.maxRating = 5;
    Rating.rating(el,{
      initialRating: binding.value,
      fireOnInit: false,
      maxRating: 5,
      clearable: 'auto',
      interactive: false,
    })
  },
};
export default RatingDirective;