import $ from 'jquery';
import Search from 'Modules/Search';

let SearchDirective = {
  inserted(el, binding) {
    (new Search()).init(el, {
      onSelect(result, response) {
        let input = $('[data-search="' + binding.arg + '"]', el)[0];
        input.value = result.name;
        input.dispatchEvent(new Event('input', {bubbles: true}));
      },
    }, 'part helper');
  }
};
export default SearchDirective;