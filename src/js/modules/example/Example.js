import $ from 'jquery';
import Hightlight from 'highlight.js';

class Example {
  constructor(selector, init = false) {
    this.selector = selector;
    this.$elements = $(selector);
    if (init) {
      this.init();
    }
  }

  init() {
    let $wrapper = $('<div class="kit example"></div>');
    this.$elements.each(function() {
      let $this = $(this);
      let $code = $('<pre class="kit example code"><code class="html">' + $this.html().replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;') + '</code></pre>');
      $this.append($code);
      Hightlight.highlightBlock($code[0]);
    });
  }

  static examples(selector) {
    let $elements = $(selector);
    $elements.each(function() {
      let $this = $(this);
      new Example($this, true);
    });
  }
}

export default Example;