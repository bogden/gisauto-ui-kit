import $ from 'jquery';

class Bar {
  constructor(selector) {
    this.$bar = !!selector ? $(selector) : $('#siteHeader');
    this.$wrapper = $('<div class="bar-wrapper"></div>');
  }

  /*** Иницализация панели ***/
  initialize() {
    if (this.$bar.hasClass('fixed')) {
      this.$bar.on('refresh', function(e) {
        this.$bar.parent().height(this.$bar.height());
      }.bind(this));
      this.$bar.css({
        position: 'fixed',
      }).wrap(this.$wrapper).trigger('refresh');
    }
    return this;
  }

  /*** Фиксировать панель или нет ***/
  fixedBar(fixed) {
    if (!!fixed) {
      this.$bar.addClass('fixed');
    } else {
      this.$bar.removeClass('fixed');
    }
    return this;
  }

  /*** Маленький размер ***/
  mini() {
    this.$bar.removeClass('big').trigger('refresh');
    return this;
  }

  /*** Большой размер ***/
  big() {
    this.$bar.removeClass('big').trigger('refresh');
    return this;
  }

  /*** Переключуть размер ***/
  toggleSize() {
    this.$bar.toggleClass('big').trigger('refresh');
    return this;
  }
}

export default Bar;