import $ from 'jquery';
import DomPluginBone from '../../bones/DomPluginBone';

class Sidebar extends DomPluginBone {
  /**
   * Sidebar constructor
   * @param sidebar
   * @param swiper
   * @param submenu
   * @param activators
   */
  constructor(sidebar, swiper, submenu, activators) {
    super();
  }

  /**
   * Toggle sidebar state
   * @return {Sidebar}
   */
  toggle() {
    this.getElements().hasClass('show')
        ? this.hide()
        : this.show();
    return this;
  }

  /**
   * Show sidemenu
   * @returns {Sidebar}
   */
  show() {
    this.getElements().addClass('show');
    $('body').css({overflow: 'hidden'});
    return this;
  }

  /**
   * @returns {Sidebar}
   */
  hide() {
    this.getElements().removeClass('show');
    $('body').css({overflow: 'auto'});
    return this;
  }

  /**
   * Change submenu
   */
  initSubmenu(){
    let $categories = this.getElements().find('.submenu .category');
    let $links = this.getElements().find('.stack .link');
    $links.on('click', function(e) {
      e.preventDefault();
      console.log(this, $(this).attr('href'));
      $categories.removeClass('active');
      console.log($categories);
      $links.removeClass('active').filter(this).addClass('active');
      $categories.removeClass('active').filter($(this).attr('href')).addClass('active');
    });
    return this;
  }

  /**
   * init in to $$ space
   * @return {Sidebar}
   */
  static load() {
    !window.hasOwnProperty('$$') && (window.$$ = {});
    !window.$$.hasOwnProperty('Sidebar') && (window.$$.Sidebar = new Sidebar());
    return window.$$.Sidebar;
  }
}

export default Sidebar;