import Preloader from 'Modules/Preloader';

$(function() {
  $.fn.preloader = function(options = {}) {
    let instance = {};
    this.each(function() {
      if (typeof options === 'object') {
        instance = new Preloader(this, options);
      } else {
        switch (options) {
          case 'show':
            instance.show();
            break;
          case 'hide':
            instance.hide();
            break;
          default:
            instance = new Preloader();
        }
      }
      instance.init();
    });
    return this;
  };
});
