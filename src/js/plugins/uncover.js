$(function() {
  $.fn.uncover = function() {
    this.each(function() {
      let $this = $(this);
      $this.on('click', function(e) {
        e.preventDefault();
        if ($this.is('.cover')) {
          $this.addClass('uncover');
        }
      })
    });
    return this;
  };
});
