export default {
  props: {
    errors: {require: true},
  },
  template: `
    <div v-if="errors">
            <div v-for="error in errors">
                {{error}}
            </div>
    </div>
`,
};
