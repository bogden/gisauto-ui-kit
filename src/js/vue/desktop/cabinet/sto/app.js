import VueRouter from 'vue-router';
import List from './List.vue';
import FormSto from './FormSto.vue';
import FormShop from './FormShop.vue';

const router = new VueRouter({
  routes: [
    {
      name: 'list',
      path: '/',
      component: List,
    }, {
      name: 'new-sto',
      path: '/new-sto',
      component: FormSto,
    }, {
      name: 'edit-sto',
      path: '/:id/edit-sto',
      component: FormSto, props: true
    }, {
      name: 'new-shop',
      path: '/new-shop',
      component: FormShop,
    }, {
      name: 'edit-shop',
      path: '/:id/edit-shop',
      component: FormShop, props: true
    },
  ],
});

const vm = new Vue({
  router,
  el: '#app',
  template: `<div id="app"><router-view></router-view></div>`,
});

export default vm;
