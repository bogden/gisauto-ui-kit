import Vue from 'vue/dist/vue.min';
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import FormInputErrorsComponent from './components/FormInputErrors';
import Multiselect from 'vue-multiselect';
import VueTheMask from 'vue-the-mask';
import draggable from 'vuedraggable';

Vue.use(VueResource);
Vue.use(VueRouter);

global.Vue = Vue;

Vue.component('form-input-errors', FormInputErrorsComponent);
Vue.component('multiselect', Multiselect);
Vue.component('draggable', draggable);
Vue.use(VueTheMask);

