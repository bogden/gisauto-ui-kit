$( function() {
  if ( !window.$$ ) {
    window.$$ = {};
  }
  window.$$.common = new Common();
  window.$$.semantic = new Semantic();

  Common.tooltip();

  $( '.bar-btn-search' ).on( 'click', function() {
    $( this ).hide( 300 );
    $( '.bar-search' ).show( 300 );
    $( '.bar-search input' ).focus();
    $( document ).trigger( 'init.input' );
  } );

  $( '.bar-search input' ).focusout( function() {
    if ( $( this ).val() === '' ) {
      $( '.bar-search' ).hide( 300 );
      $( '.bar-btn-search' ).show();
    }
  } );

  $( '#sidebar-close' ).on( 'click', function() {
    $( '#sidebar' ).toggleClass( 'm-active' );
  } );
  $( '.sidebar__link' ).on( 'click', function( e ) {
    if ( $( '#sidebar' ).hasClass( 'm-active' ) ) $( '#sidebar' ).removeClass( 'm-active' );
  } );

  $( '.btn-toggle-panel' ).on( 'click', function() {
    if ( $( this ).hasClass( 'active' ) ) return;
    $( '.btn-toggle-panel' ).toggleClass( 'active' );
    $( '.panel-50' ).removeClass( 'active' );
    var id = '#' + $( this ).data( 'toggle' );
    $( id ).addClass( 'active' );
  } );

  $( '#head-description.text-collapse' ).textCollapse( {
    revealText: 'Показать ещё...',
    hideText: 'Свернуть',
    linesNumber: 2,
    hideMore: false,
    dots: false
  } );
} );
