$(function() {
  window.$$.dropdown = new Dropdown();
  $(document)
    .on('init.inputs init.dropdown', function(e) {
      window.$$.dropdown.init('.ui.dropdown.auto-init', {
        values: false,
        allowAdditions: false,
        useLabels: false,
        onChange: Dropdown.onChange
      });
    }).trigger('init.dropdown');
});
