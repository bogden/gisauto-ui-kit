$(function() {
  window.$$.inputManipulator = new InputManipulators();
  window.$$.inputManipulator
    .initNumbersInput('input[type="number"]')
    .eraseControl('.form-control-erase')
    .passwordControl('.form-control-password')
    .phoneControl('.form-control-phone')
    .emailControl('.form-control-email');
  // FIXTURES
  InputManipulators.fixtures();
});
