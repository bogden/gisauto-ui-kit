const gulp = require('gulp');
const gulpif = require('gulp-if');
const sass = require('gulp-sass');
const sassInlineSvg = require('gulp-svg-inline-scss');
const autoprefixer = require('gulp-autoprefixer');
const cssmin = require('gulp-cssmin');
const svgmin = require('gulp-svgmin');
const csso = require('gulp-csso');
const path = require('path');
const fs = require('fs');
const console = require('better-console');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const inlineFonts = require('gulp-inline-fonts');
const colors = require('colors');

let ROOT_DIR = process.env.ROOT_DIR || process.cwd();
let PACKAGE_DIR = process.env.PACKAGE_DIR || path.resolve(__dirname, '../../');

module.exports = class buildCss {
  static cssSass() {
    let config = require('../config/config')(process.env.BUILD_CONF).css;
    console.info('[' + ('Task'.grey) + '] Генерация стилей с помощью SASS препроцессора'.green + '\n' +
        '[' + ('Source'.grey) + '] ' + path.resolve(PACKAGE_DIR, config.path.sass.src + config.path.sass.files) + '\n' +
        '[' + ('Output'.grey) + '] ' + path.resolve(ROOT_DIR, config.path.sass.output));
    let stream = gulp.src(
        [path.resolve(PACKAGE_DIR, config.path.sass.src + config.path.sass.files)].concat(config.path.sass.exclude)).
        pipe(sass().on('error', sass.logError)).
        pipe(autoprefixer(config.autoPrefixer)).
        pipe(csso({
          restructure: true,
          sourceMap: false,
          debug: false,
        })).
        pipe(gulp.dest(path.resolve(ROOT_DIR, config.path.sass.output)));

    if (config.minify) {
      stream.
          pipe(gulpif(config.minify, cssmin())).
          pipe(gulpif(config.minify, rename({suffix: '.min', prefix: ''}))).
          pipe(gulpif(config.minify, gulp.dest(path.resolve(ROOT_DIR, config.path.sass.output))));
    }

    return stream;
  }

  static cssInlineSvg() {
    let config = require('../config/config')(process.env.BUILD_CONF).css;
    if (!fs.existsSync(path.resolve(PACKAGE_DIR, config.path.inlineSvg.src))) {
      console.warn('Папка SVG на найдена');
      return null;
    }
    console.info('[' + ('Task'.grey) + '] Генерация \"Inline SVG file\"'.green + '\n' +
        '[' + ('Source'.grey) + '] ' + path.resolve(PACKAGE_DIR, config.path.inlineSvg.src + config.path.inlineSvg.files) + '\n' +
        '[' + ('Output'.grey) + '] ' + path.resolve(PACKAGE_DIR, config.path.inlineSvg.output));
    return gulp.src(path.resolve(PACKAGE_DIR, config.path.inlineSvg.src + config.path.inlineSvg.files)).
        pipe(svgmin()).
        pipe(sassInlineSvg({
          destDir: path.resolve(PACKAGE_DIR, config.path.inlineSvg.output),
        }));
  }

  static cssInlineFont() {
    let config = require('../config/config')(process.env.BUILD_CONF).css;
    let stream = null;
    let tableData = [];
    if (!fs.existsSync(path.resolve(PACKAGE_DIR, config.path.inlineFont.src))) {
      console.warn('Папка шрифтов не найдена');
      return null;
    } else {
      console.info('[' + ('Task'.grey) + ']' + (' Генерация \"Inline Fonts file\"'.green) + '\n' +
          '[' + ('Source'.grey) + '] ' + path.resolve(PACKAGE_DIR, config.path.inlineFont.src) + '\n' +
          '[' + ('Output'.grey) + '] ' + path.resolve(PACKAGE_DIR, config.path.inlineFont.output));
      fs.readdirSync(path.resolve(PACKAGE_DIR, config.path.inlineFont.src)).filter(function(file) {
        return fs.statSync(path.resolve(PACKAGE_DIR, config.path.inlineFont.src, file)).isFile() && file.search(/^\./) === -1;
      }).map(function(file) {
        let weightSearch = file.toString().
            match(/(Black|black|Bold|bold|Light|light|Medium|medium|Regular|regular|Thin|thin)/);
        let fontStyle = file.toString().match(/(Italic|italic)/) ? 'italic' : 'normal';
        let weightTable = {
          black: 900,
          bold: 700,
          medium: 500,
          regular: 400,
          light: 300,
          thin: 200,
        };
        let nameMatch = file.match(/^(\w+)/);
        let name = nameMatch ? nameMatch.shift() : file;
        let weight = weightSearch ? weightTable[weightSearch.shift().toLowerCase()] : 400;
        stream = gulp.src(path.resolve(PACKAGE_DIR, config.path.inlineFont.src + '/' + file)).pipe(inlineFonts({
          name: name,
          style: fontStyle,
          weight: weight,
          formats: ['woff', 'woff2', 'ttf'],
        })).pipe(rename({
          basename: file.split('.')[0],
          prefix: '_',
          suffix: '',
          extname: '.scss',
        })).pipe(gulp.dest(path.resolve(PACKAGE_DIR, config.path.inlineFont.output)));

        tableData.push({
          font: name,
          style: weight.toString().bold + ' ' + fontStyle,
          path: path.resolve(PACKAGE_DIR, config.path.inlineFont.output)
        });
      });
    }
    console.table(tableData);
    return stream;
  }

};
