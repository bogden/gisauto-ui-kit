const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const path = require('path');
const console = require('better-console');
const colors = require('colors');

let ROOT_DIR = process.env.ROOT_DIR || process.cwd();
let PACKAGE_DIR = process.env.PACKAGE_DIR || path.resolve(__dirname, '../../');

module.exports = class building {
  static mobileSass() {
    let config = require('../config/config')(process.env.BUILD_CONF).css;
    console.info('[' + ('Task'.grey) + ']' + ' Генерация стилей для мобильной версии'.green + '\n' +
        '[' + ('Source'.grey) + '] ' + path.resolve(PACKAGE_DIR, config.path.sass.src + config.path.sass.files) + '\n' +
        '[' + ('Output'.grey) + '] ' + path.resolve(ROOT_DIR, config.path.sass.output));
    return gulp.src(
        path.resolve(PACKAGE_DIR, config.path.sass.src, 'mobile.scss' )).
        pipe(sass().on('error', sass.logError)).
        pipe(autoprefixer(config.autoPrefixer)).
        pipe(gulp.dest(path.resolve(ROOT_DIR, config.path.sass.output)));
  }
};
