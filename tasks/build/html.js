const gulp = require('gulp');
const colors = require('colors');
const gulpif = require('gulp-if');
const fs = require('fs');
const twig = require('gulp-twig');
const path = require('path');
const console = require('better-console');

let ROOT_DIR = process.env.ROOT_DIR || process.cwd();
let PACKAGE_DIR = process.env.PACKAGE_DIR || path.resolve(__dirname, '../../');

module.exports = class buildHTML {
  static twig() {
    let config = require('../config/config')(process.env.BUILD_CONF).html;
    return gulp.src(path.resolve(PACKAGE_DIR, config.path.twig.src + config.path.twig.files)).pipe(twig({
      functions: [
        {
          name: 'pagelist',
          func: function() {
          },
        },
        {
          name: 'example',
          func: function(value, params) {
            return '<div class="example">' + value + '</div>';
          },
        },
      ],
    })).on('error', function(error) {
      console.error(error.message.red);
    }).pipe(gulp.dest(path.resolve(ROOT_DIR, config.path.twig.output)));
  }

  static json() {
    let config = require('../config/config')(process.env.BUILD_CONF).html;
    if (!fs.existsSync(path.resolve(PACKAGE_DIR, config.path.json.src))) {
      console.log('Папка с json не найдена'.yellow);
      return null;
    } else {
      console.log('Перенос json файлов'.green);
      return gulp.src(path.resolve(PACKAGE_DIR, config.path.json.src + config.path.json.files)).
          pipe(gulp.dest(path.resolve(ROOT_DIR, config.path.json.output)));
    }
  }
};
