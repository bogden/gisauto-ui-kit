const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const colors = require('colors');
const gulpif = require('gulp-if');
const fs = require('fs');
const rename = require('gulp-rename');
const runTimestamp = Math.round(Date.now() / 1000);
const svgmin = require('gulp-svgmin');
const iconfont = require('gulp-iconfont');
const console = require('better-console');
const path = require('path');

let ROOT_DIR = process.env.ROOT_DIR || process.cwd();
let PACKAGE_DIR = process.env.PACKAGE_DIR || path.resolve(__dirname, '../../');

module.exports = class Media {
  static images() {
    let config = require('../config/config')(process.env.BUILD_CONF).media;
    if (!fs.existsSync(path.resolve(PACKAGE_DIR, config.path.images.src))) {
      console.warn('Папка с изображениями не найдена');
      return null;
    } else {
      console.info('[' + ('Task'.grey) + '] ' + 'Подготовка изображений'.green + '\n' +
          '[' + ('Source'.grey) + '] ' + path.resolve(PACKAGE_DIR, config.path.images.src + config.path.images.files) +
          '\n' +
          '[' + ('Output'.grey) + '] ' + path.resolve(ROOT_DIR, config.path.images.output));
      return gulp.src(path.resolve(ROOT_DIR, config.path.images.src + config.path.images.files)).
          pipe(gulpif(config.imagemin, imagemin())).
          pipe(rename(function(path) {
            path.basename = path.basename.toLowerCase();
          })).
          pipe(gulp.dest(path.resolve(ROOT_DIR, config.path.images.output)));
    }
  }

  static videos() {
    let config = require('../config/config')(process.env.BUILD_CONF).media;
    if (!fs.existsSync(path.resolve(PACKAGE_DIR, config.path.video.src))) {
      console.warn('Папка для видео не найдена');
      return null;
    } else {
      console.log('[' + ('Task'.grey) + '] ' + 'Подготовка видео'.green);
      return gulp.src(path.resolve(PACKAGE_DIR, config.path.video.src + config.path.video.output)).
          pipe(rename(function(path) {
            path.basename = path.basename.toLowerCase();
          })).
          pipe(gulp.dest(path.resolve(ROOT_DIR, config.path.video.output)));
    }
  }

  static fonts() {
    let config = require('../config/config')(process.env.BUILD_CONF).media;
    if (!fs.existsSync(path.resolve(PACKAGE_DIR, config.path.fonts.src))) {
      console.warn('Папка для шрифтов не найдена'.yellow);
      return null;
    } else {
      console.info('[' + ('Task'.grey) + '] ' + 'Подготовка шрифтов'.green + '\n' +
          '[' + ('Source'.grey) + '] ' + path.resolve(PACKAGE_DIR, config.path.fonts.src) + config.path.fonts.files +
          '\n' +
          '[' + ('Output'.grey) + '] ' + path.resolve(ROOT_DIR, config.path.fonts.output));
      return gulp.src(path.resolve(PACKAGE_DIR, config.path.fonts.src + config.path.fonts.files)).
          pipe(rename(function(path) {
            path.basename = path.basename.toLowerCase();
          })).
          pipe(gulp.dest(path.resolve(ROOT_DIR, config.path.fonts.output)));
    }
  }

  static iconFonts() {
    let config = require('../config/config')(process.env.BUILD_CONF).media;
    let iconFontsPath = path.resolve(PACKAGE_DIR, config.path.iconFonts.src);
    let stream = null;
    if (!fs.existsSync(iconFontsPath)) {
      console.warn(iconFontsPath.bold + ' - Не найден путь.');
      return null;
    }
    console.info('[' + ('Task'.grey) + '] ' + 'Создавние иконочных шрифтов'.green + '\n' +
        '[' + ('Source'.grey) + '] ' + iconFontsPath + '\n' +
        '[' + ('Output'.grey) + '] ' + path.resolve(PACKAGE_DIR, config.path.iconFonts.output) +
        '╔ Файлы иконок');
    fs.readdirSync(iconFontsPath).filter(function(file) {
      return fs.statSync(path.join(iconFontsPath, file)).isDirectory();
    }).map(function(folder) {
      console.info(' ╠ ' + path.join(iconFontsPath, folder, config.path.iconFonts.files));
      stream = gulp.src(path.join(iconFontsPath, folder, config.path.iconFonts.files)).
          pipe(svgmin()).
          pipe(iconfont({
            fontName: folder,
            prependUnicode: true,
            formats: ['woff2'],
            timestamp: runTimestamp,
            normalize: true,
            fontHeight: 1000,
          })).
          on('glyphs', function(glyphs, options) {
            let table = [];
            glyphs.forEach(function(current) {
              table.push({file: current.name.yellow, unicode: (current.unicode).toString()});
            });
            console.info(('Шрифт "' + options.fontName + '" создан ').cyan)
            console.table(table);
          }).
          pipe(rename(function(path) {
            path.basename = path.basename.toLowerCase();
          })).
          pipe(gulp.dest(path.resolve(PACKAGE_DIR, config.path.iconFonts.output)));
    });
    return stream;
  }
};
