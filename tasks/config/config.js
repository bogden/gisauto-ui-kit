const json = require('jsonfile');
const path = require('path');
const colors = require('colors');
const console = require('better-console');

let ROOT_DIR = process.env.ROOT_DIR || process.cwd();

let distConfig = json.readFileSync(path.join(ROOT_DIR, './config.json'));
let packageConfig = json.readFileSync(path.join(ROOT_DIR, './package.json'));

module.exports = function(mode = 'local', options = {}) {
  console.info('\nЗагружена ' + (mode.bold) + ' конфигурация\n');
  return {
    dist: distConfig,
    version: packageConfig.version,
    css: {
      path: {
        sass: {
          files: ['/**/*.scss'],
          exclude: mode === 'local' ? [] : [
            '!.src/scss/icons/_sass-inline-svg-data.scss',
            '!./**/example.scss',
          ],
          src: './src/scss',
          output: path.join(distConfig.path.output[mode], '/css'),
        },
        inlineSvg: {
          files: '/**/*.svg',
          src: './src/svg',
          output: './src/scss/icons',
        },
        inlineFont: {
          files: '/**/*',
          src: './src/fonts',
          output: './src/scss/fonts',
        },
      },
      minify: true,
      sourceMaps: true,
      autoPrefixer: {
        browsers: ['last 15 versions'],
        cascade: false,
      },
      sync: true,
    },
    html: {
      path: {
        twig: {
          files: '/**/*.twig',
          src: './src/html',
          output: './dist',
        },
        json: {
          files: '/*.json',
          src: './src/json',
          output: path.join(distConfig.path.output[mode], '/json'),
        },
      },
    },
    media: {
      path: {
        images: {
          files: '/**/*.+(jpg|jpeg|gif|png|svg)',
          src: './src/images',
          output: path.join(distConfig.path.output[mode], '/images'),
        },
        video: {
          files: '/**/*.+(mp4|mov)',
          src: './src/videos',
          output: path.join(distConfig.path.output[mode], '/videos'),
        },
        sprites: {
          files: '/*.+(jpg|jpeg|gif|png)',
          src: './src/sprites',
          output: path.join(distConfig.path.output[mode], '/sprites'),
        },
        fonts: {
          files: '/**/*.+(ttf|woff|woff2|otf|svg)',
          src: './src/fonts',
          output: path.join(distConfig.path.output[mode], '/fonts'),
        },
        iconFonts: {
          files: '/*.svg',
          src: './src/iconfonts',
          output: './src/fonts',
        },
      },
      imagemin: true,
      svgmin: true,
      sync: false,
    },
    server: {
      server: './dist',
      open: false,
      notify: false,
      ghostMode: false,
      ui: false,
      socket: {
        domain: 'gisauto.test:3000',
      },
    },
  };
};