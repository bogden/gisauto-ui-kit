global.server = require('browser-sync');
module.exports = class devServer {
  static start() {
    global.server({
      server: './dist',
      open: false,
      notify: false,
      ghostMode: false,
      ui: false
    });
  }
};
