const gulp = require('gulp');
const console = require('better-console');
const extend = require('extend');
const fs = require('fs');
const path = require('path');
const prompt = require('prompt');
const jsonfile = require('jsonfile');
const rename = require('gulp-rename');
const webpack = require('webpack');
const runGulpTask = require('run-gulp-task');

let ROOT_DIR = process.env.ROOT_DIR || process.cwd();
let PACKAGE_DIR = process.env.PACKAGE_DIR || path.resolve(__dirname, '../../');

process.chdir(PACKAGE_DIR);
const install = function() {
  return Promise.all([
    new Promise(resolve => {
      console.log(('\nУстановка UI Kit Запущена\n'.bold)
          + ('process: '.bold).blue + process.cwd() + '\n'
          + ('ROOT_DIR: '.bold).blue + ROOT_DIR + '\n'
          + ('PACKAGE_DIR: '.bold).blue + PACKAGE_DIR + '\n');

      if (!fs.existsSync(path.resolve(PACKAGE_DIR, 'config.example.json'))) {
        console.warn('Первичный файл конфигурации не найден'.red + '\n');
        return process.exit(1);
      }

    }),
    new Promise(resolve => {
      let config;
      if (fs.existsSync(path.resolve(ROOT_DIR, 'config.json'))) {
        console.log('Файл конфигурации существует\n'.green);
        config = jsonfile.readFileSync(path.resolve(ROOT_DIR, 'config.json'));
      } else {
        config = jsonfile.readFileSync(path.resolve(PACKAGE_DIR, 'config.example.json'));
        console.log('Укажите пути для сборки файлов.');
        prompt.start();
        prompt.get([
          {
            name: 'productionBuildPath',
            required: true,
            default: config.path.output.production,
            message: 'Куда собирать production',
          }, {
            name: 'testBuildPath',
            required: true,
            default: config.path.output.local,
            message: 'Куда собирать local',
          }], function(err, result) {
          console.log('Папка для сборки:'.green);
          console.log('Production build: ' + result.productionBuildPath);
          console.log('Test build: ' + result.testBuildPath);
          config.path.output.production = result.productionBuildPath;
          config.path.output.local = result.testBuildPath;
          jsonfile.writeFileSync(path.resolve(ROOT_DIR, 'config.json'), config);
        });
      }
    }),
    new Promise(resolve => {
      if (!fs.existsSync(path.resolve(ROOT_DIR, 'gulpfile.js'))) {
        gulp.src('./tunel.js').
            pipe(rename(function(path) {
              path.basename = 'gulpfile';
            })).
            pipe(gulp.dest(ROOT_DIR));
        console.info('gulpfile.js создан в ' + ROOT_DIR);
      }
    }),
    new Promise(resolve => webpack(require('../webpack/prod/webpack'), (err, stats) => {
      if (err) console.log('\nWebpack', err);
      console.log(stats.toString({}));
      process.chdir(ROOT_DIR);
      resolve();
    })).then(function() {
      process.env.BUILD_CONF = 'production';
      gulp.start('build production');
    }),
  ]).then(function() {
    return process.exit(0);
  });

};

module.exports = install;