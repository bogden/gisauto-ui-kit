const path = require('path');
const webpack = require('webpack');
const cTable = require('console.table');
const glob = require('glob');
const _ = require('lodash');
const colors = require('colors');

let ROOT_DIR = process.env.ROOT_DIR || process.cwd();
let PACKAGE_DIR = process.env.PACKAGE_DIR || path.resolve(__dirname, '../../../');

module.exports = {
  entry: function() {
    let files = glob.sync(path.resolve(PACKAGE_DIR,'./src/js/_entry/**/*.js'), {});
    let entry = {};
    let table = [];
    console.log('Автоматически найденные скриты:'.magenta);
    files.forEach(function(filePath) {
      let name = _.snakeCase(filePath.replace('\\', '/').replace(path.resolve(PACKAGE_DIR,'./src/js/_entry/'), '').replace(/\.js$/, ''));
      entry[name] = filePath;
      table.push({entry: name.cyan, file: filePath.yellow});
    });
    console.table(table);
    return entry;
  }(),
  output: {
    path: path.resolve(PACKAGE_DIR, './dist/js'),
    filename: '[name].js',
    publicPath: '/js/',
    library: '[name]',
  },
  resolve: {
    modules: [
      path.join(__dirname, 'src/js/components'),
      'bower_components',
      'node_modules',
      path.join(__dirname, 'src/semantic/src/definitions/modules'),
    ],
    alias: {
      Bower: path.resolve(PACKAGE_DIR, './bower_components'),
      Modules: path.resolve(PACKAGE_DIR, './src/js/modules'),
      Semantic: path.resolve(PACKAGE_DIR, './src/semantic/src/definitions/modules'),
      SemanticBehaviors: path.resolve(PACKAGE_DIR, './src/semantic/src/definitions/behaviors'),
      VueModules: path.resolve(PACKAGE_DIR, './src/js/vue')
    },

    extensions: ['.es6', '.js', '.jsx', '.vue'],
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: 'vue-style-loader!css-loader!sass-loader',
            sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax', // <style lang="sass">
          },
        },
      },
      {
        test: /\.js|es6/,
        exclude: /node_modules|bower_components/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  'env', {
                  targets: {
                    uglify: true,
                  },
                },
                ]],
              env: {
                'staging': {
                  'presets': [
                    'minify',
                  ],
                },
                'production': {
                  'presets': [
                    'minify',
                  ],
                },
              },
            },
          },
        ],
      },
    ],
  },
  devtool: 'source-map',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    port: 4000,
    hot: true,
  },
  plugins: [
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery',
      'window.jQuery': 'jquery',
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
      },
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],
  stats: {},
};
