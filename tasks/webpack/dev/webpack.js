const path = require('path');
const config = require('../common/webpack');
const colors = require('colors');
const gulpPlugin = require('../plugins/assetsGulpPlugin');
const merge = require('webpack-merge');

console.log('========================================='.green);
console.log('=                                       ='.green);
console.log('=        Сборка JS для Symfony          ='.green);
console.log('=                                       ='.green);
console.log('========================================='.green);

module.exports = merge(config, {
  output: {
    path: path.resolve('../gisauto/web/assets/js')
  },
  plugins: [],
});