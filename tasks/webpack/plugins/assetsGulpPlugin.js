const path = require('path');
const colors = require('colors');
const runGulpTask = require('run-gulp-task');
const execSync = require('child_process').execSync;

class BuildAssets {
  apply(compiler) {
    compiler.plugin('compile', function(params) {});
    compiler.plugin('done', function(stats) {
      return Promise.all([
        new Promise(function() {
          console.log('Start assets build...'.yellow);
          process.env.BUILD_CONF = 'production';
          runGulpTask('build production', path.join(process.cwd(), 'gulpfile.js'));
        })
      ]);
    });
  }
}

module.exports = BuildAssets;