const path = require('path');
const webpack = require('webpack');
const config = require('../common/webpack');
const colors = require('colors');
const MinifyPlugin = require("babel-minify-webpack-plugin");
const merge = require('webpack-merge');
const jsonfile = require('jsonfile');

let ROOT_DIR = process.env.ROOT_DIR || require('app-root-dir').get();
let output = path.resolve(ROOT_DIR, jsonfile.readFileSync(path.resolve(ROOT_DIR,'config.json')).path.output.production || '../gisauto/web/assets', 'js');

console.log('========================================='.magenta);
console.log('=                                       ='.magenta);
console.log('=        Сборка JS для Symfony          ='.magenta);
console.log('=          (Production mode)            ='.magenta);
console.log('=                                       ='.magenta);
console.log('========================================='.magenta);
console.log('\nПапка для cборки JS: ' + output.magenta);

module.exports = merge(config, {
  output: {
    path: path.resolve(output),
  },
  plugins: [
    new MinifyPlugin({
      removeConsole: true
    },{
      sourceMap: false
    }),
  ],
})
;